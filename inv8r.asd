(asdf:defsystem #:inv8r
  :depends-on (:xelf)
  :components ((:file "package")
	       (:file "station" :depends-on ("package"))
	       (:file "inv8r" :depends-on ("station"))))

