;;; inv8r.lisp --- you versus the robots

;; Copyright (C) 2014-2016, 2021  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: games

(in-package :inv8r)

(defvar *gradient* nil)
(defvar *triggers* nil)

;;; Utility functions

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

(defun sample-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.wav" name n)))

(defparameter *sprout-images* (image-set "sprout" 6))

(defparameter *bip-sounds* (sample-set "bip" 4))
(defresource "bip-1.wav" :volume 30)
(defresource "bip-2.wav" :volume 30)
(defresource "bip-3.wav" :volume 30)
(defresource "bip-4.wav" :volume 30)

(defun random-bip-sound () (random-choose *bip-sounds*))

(defresource "barrier.png" :filter :nearest)
(defresource "barrier2.png" :filter :nearest)

(defparameter *rez-blurs* (image-set "rez-blur" 5))
(defparameter *rez-lights* (image-set "rez-light" 5))

;;; Music and sound resources

(defparameter *default-font* "sans-mono-bold-12")
(defparameter *small-font* "sans-mono-bold-10")
(defparameter *big-font* "sans-mono-bold-18")
(defvar *music* t)

(defparameter *respiration-sounds* (sample-set "respiration" 6))
(defresource "respiration-1.wav" :volume 20)
(defresource "respiration-2.wav" :volume 20)
(defresource "respiration-3.wav" :volume 20)
(defresource "respiration-4.wav" :volume 20)
(defresource "respiration-5.wav" :volume 20)
(defresource "respiration-6.wav" :volume 20)

(defparameter *pulse-sounds* (sample-set "pulse" 6))
(defresource "pulse-1.wav" :volume 30)
(defresource "pulse-2.wav" :volume 30)
(defresource "pulse-3.wav" :volume 30)
(defresource "pulse-4.wav" :volume 30)
(defresource "pulse-5.wav" :volume 30)
(defresource "pulse-6.wav" :volume 30)

(defresource "alarm.wav" :volume 30)
(defresource "bombs-away.wav" :volume 70)
(defresource "bigboom.wav" :volume 140)
(defresource "bip.wav" :volume 70)
(defresource "blurp.wav" :volume 70)
(defresource "grow.wav" :volume 70)
(defresource "munch1.wav" :volume 70)
(defresource "geiger2.wav" :volume 25)
(defresource "xplod.wav" :volume 30)
(defresource "bleep.wav" :volume 70)
(defresource "bloop.wav" :volume 70)
(defresource "bounce.wav" :volume 10)
(defresource "newball.wav" :volume 20)
(defresource "return.wav" :volume 20)
(defresource "error.wav" :volume 40)
(defresource "buzz.wav" :volume 30)
(defresource "chevron.wav" :volume 70)
(defresource "death-alien.wav" :volume 100)
(defresource "death.wav" :volume 70)
(defresource "error-old1.wav" :volume 70)
(defresource "error.wav" :volume 70)
(defresource "freeze.wav" :volume 70)
(defresource "gate-closing-sound.wav" :volume 30)
(defresource "gate-closing-sound2.wav" :volume 30)
(defresource "go.wav" :volume 70)
(defresource "grab.wav" :volume 30)
(defresource "hole.wav" :volume 70)
(defresource "lock-opening-sound.wav" :volume 70)
(defresource "muon-fire.wav" :volume 120)
(defresource "phasebell.wav" :volume 70)
(defresource "plasma.wav" :volume 70)
(defresource "powerup.wav" :volume 70)
(defresource "powerdown.wav" :volume 20)
(defresource "serve.wav" :volume 70)
(defresource "shield-warning.wav" :volume 170)
(defresource "shield.wav" :volume 170)
(defresource "talk.wav" :volume 70)
(defresource "turn.wav" :volume 70)
(defresource "upwoop.wav" :volume 70)
(defresource "worp.wav" :volume 70)
(defresource "xeng.wav" :volume 70)
(defresource "zap1.wav" :volume 70)
(defresource "zap2.wav" :volume 70)
(defresource "zap3.wav" :volume 70)
(defresource "zap4.wav" :volume 70)
(defresource "zap5.wav" :volume 70)
(defresource "zap6.wav" :volume 70)
(defresource "zap7.wav" :volume 70)
(defresource "yelp.wav" :volume 20)

(defresource 
      (:name "boop1.wav" :type :sample :file "boop1.wav" :properties (:volume 20))
      (:name "boop2.wav" :type :sample :file "boop2.wav" :properties (:volume 20))
    (:name "boop3.wav" :type :sample :file "boop3.wav" :properties (:volume 20)))

(defparameter *bounce-sounds* '("boop1.wav" "boop2.wav" "boop3.wav"))

(defresource 
    (:name "doorbell1.wav" :type :sample :file "doorbell1.wav" :properties (:volume 23))
    (:name "doorbell2.wav" :type :sample :file "doorbell2.wav" :properties (:volume 23))
  (:name "doorbell3.wav" :type :sample :file "doorbell3.wav" :properties (:volume 23)))

(defparameter *doorbell-sounds* '("doorbell1.wav" "doorbell2.wav" "doorbell3.wav"))

(defparameter *slam-sounds*
  (defresource 
      (:name "slam1.wav" :type :sample :file "slam1.wav" :properties (:volume 52))
      (:name "slam2.wav" :type :sample :file "slam2.wav" :properties (:volume 52))
    (:name "slam3.wav" :type :sample :file "slam3.wav" :properties (:volume 52))))

(defresource 
    (:name "whack1.wav" :type :sample :file "whack1.wav" :properties (:volume 42))
    (:name "whack2.wav" :type :sample :file "whack2.wav" :properties (:volume 42))
  (:name "whack3.wav" :type :sample :file "whack3.wav" :properties (:volume 42)))

(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))

(defresource 
    (:name "color1.wav" :type :sample :file "color1.wav" :properties (:volume 32))
    (:name "color2.wav" :type :sample :file "color2.wav" :properties (:volume 32))
  (:name "color3.wav" :type :sample :file "color3.wav" :properties (:volume 32)))

(defparameter *color-sounds* '("color1.wav" "color2.wav" "color3.wav"))

(defresource 
    (:name "blaagh.wav" :type :sample :file "blaagh.wav" :properties (:volume 42))
    (:name "blaagh2.wav" :type :sample :file "blaagh2.wav" :properties (:volume 42))
  (:name "blaagh3.wav" :type :sample :file "blaagh3.wav" :properties (:volume 42))
  (:name "blaagh4.wav" :type :sample :file "blaagh3.wav" :properties (:volume 42)))

(defparameter *blaagh-sounds* '("blaagh.wav" "blaagh2.wav" "blaagh3.wav" "blaagh4.wav"))

(defresource "analog-death.wav" :volume 170)

(defresource "vixon.ogg" :volume 30)
(defresource "victory.ogg" :volume 40)
(defresource "boss.ogg" :volume 80)
(defresource "extralife.wav" :volume 60)

(defresource "tension-1.ogg" :volume 75)
(defresource "tension-2.ogg" :volume 75)
(defresource "tension-3.ogg" :volume 75)
(defresource "tension-4.ogg" :volume 75)
(defresource "tension-5.ogg" :volume 75)
(defresource "tension-6.ogg" :volume 75)
(defresource "tension-7.ogg" :volume 75)
(defresource "tension-8.ogg" :volume 75)
(defresource "tension-9.ogg" :volume 75)
(defresource "tension-10.ogg" :volume 75)

(defparameter *tension-sounds* '("tension-1.ogg" "tension-2.ogg" "tension-5.ogg" "tension-6.ogg" "tension-7.ogg" "tension-8.ogg" "tension-10.ogg"))

;;; Globals

(defparameter *width* 1280)
(defparameter *height* 720)
(setf xelf:*unit* 16)
(defun width-in-units () (truncate (/ *width* *unit*)))
(defun height-in-units () (truncate (/ *height* *unit*)))
(setf xelf:*terminal-bottom* (- *height* (units 1.5)))
(setf xelf:*prompt-font* xelf:*terminal-font*)
(setf xelf:*terminal-left* (units 10.4))
(defun terminal-showing-p () (plusp xelf::*terminal-timer*))

(defparameter *margin* (units 3))

(defvar *arena* nil)
(defun arena () *arena*)

(defvar *agent* nil)
(defun agent () *agent*)
(defun set-agent (x) (setf *agent* x))
(defun make-agent ()
  (setf *agent* (make-instance 'hero)))
(defun agent-p (x) (eq (find-object x) (find-object *agent*)))

(defvar *sidekick* nil)
(defun sidekick () *sidekick*)
(defun set-sidekick (x) (setf *sidekick* x))
(defun make-sidekick ()
  (setf *sidekick* (make-instance 'sidekick)))
(defun sidekick-p (x) (eq (find-object x) (find-object *sidekick*)))

(defparameter *player-hearing-distance* 650)
(defparameter *maximum-oxygen* 100)
(defparameter *normal-pulse-rate* 12)
(defparameter *normal-respiration-rate* 10)
(defparameter *elevated-pulse-rate* 30)
(defparameter *high-pulse-rate* 50)

(defparameter *boss-level* 59)
(defvar *any-joystick* t)

(defparameter *player-1-joystick* 0)
(defparameter *player-2-joystick* nil)

(defun netplay-p () *netplay*)
(defvar *co-op-p* nil)
(defun co-op-p () *co-op-p*)
(defun local-p () (null *netplay*))
(defun local-co-op-p () (and (co-op-p) (local-p)))

(defvar *phosphor* t)
(defvar *label* nil)
(defvar *cached-quadtree* nil)
(defvar *paused* nil)

(defparameter *darkness* nil)

(defun dark-level-p (level) 
  (and
   (not (= *boss-level* *level*))
   (> level 7)
   (< level 40)
   (zerop (mod level (random-choose '(3 3 2))))
   (random-choose '(t t nil))))

(defvar *highest-level* 0)
(defun extra-life-level-p (level)
  (and
   (not (= *highest-level* level))
   (> level 7)
   (zerop (mod (+ level 2) 5))))

(defun security-level-p (level)
  (and
   (not (= *boss-level* level))
   (> level 5)
   (zerop (mod level 2))
   (not (darkness-p (arena)))))

(defparameter *retries* 2)
(defparameter *retry-clock* 0)
(defparameter *retry-frames* 200)
(defun start-retry-clock () (setf *retry-clock* *retry-frames*))
(defun retry-ready-p () (zerop *retry-clock*))
(defun update-retry-clock () (when (plusp *retry-clock*) (decf *retry-clock*)))
(defun lives-remaining () (1+ *retries*))
(defun last-life-p () (zerop *retries*))
(defun lose-life () (decf *retries*))
(defun game-over-p () (minusp *retries*))
(defun reset-lives () (setf *retries* 2))
(defun extra-life ()
  (when (not (= *highest-level* *level*))
    (play-sample "extralife.wav")
    (incf *retries*)
    (setf *label* "EXTRA LIFE!")
    (setf *highest-level* *level*)))

(defparameter *sweep* nil)
(defparameter *splash* t)

(defmethod contains-p ((buffer buffer) (node node))
  (block f
    (do-nodes (n buffer)
      (when (eq n node)
	(return-from f t)))))

(defparameter *inv8r-copyright-notice*
"
               ####         
# #   # #   # #    # ####  
# ##  # #   # #    # #   # 
# # # # #   #  ####  #   # 
# #  ## #   # #    # ####  
# #   #  # #  #    # #   #  
# #   #   #    ####  #    # 
-----------------------------------------------------------------
Welcome to inv8r. 
inv8r and Xelf are Copyright (C) 2006-2016 by David T. O'Toole 
email: <dto@xelf.me>   website: http://xelf.me/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Full license text of the GNU Lesser General Public License is in the
enclosed file named 'COPYING'. Full license texts for compilers,
assets, libraries, and other items are contained in the LICENSES
directory included with this application.
----------------------------------------------------------------- 
Use arrow keys (or numpad) to move, and the Shift key to fire.
")

;;; Level progression 

(defparameter *level* 0)

(defun victory-level-p ()
  (= 60 *level*))

(defun by-level (&rest args)
  (let ((index (truncate (/ *level* 7.5))))
    (if (<= (length args) index)
	(nth (- (length args) 1) args)
	(nth index args))))

(defun by-level* (&rest args)
  (+ (* *level* 0.3)
     (* 1.03 (apply #'by-level args))))

;;; Base class

(defparameter *volatile-period* 8)

(defclass thing (xelf:node)
  ((obstacle-p :initform nil :initarg obstacle-p :accessor obstacle-p)
   (direction :initform :up :accessor direction)
   (volatile-clock :initform *volatile-period* :accessor volatile-clock :initarg :volatile-clock)
   (heading :initform 0.0 :accessor heading :initarg :heading)
   (shield-clock :initform 200 :accessor shield-clock)
   (data :initform nil :accessor data :initarg :data)
   ;; pathfinding
   (path :initform nil :accessor path)
   (waypoints :initform nil :accessor waypoints)
   (goal-x :initform nil :accessor goal-x)
   (goal-y :initform nil :accessor goal-y)))

(defmethod find-colliding-objects ((self thing))
  (loop for object being the hash-keys of (field-value :objects (current-scene))
	when (colliding-with self (find-object object))
	  collect (find-object object)))

(defmethod next-waypoint ((self thing))
  (with-slots (waypoints goal-x goal-y) self 
    (destructuring-bind (wx wy) (pop waypoints)
      (setf goal-x wx goal-y wy))))

(defmethod walk-toward ((self thing) x y)
  (with-slots (goal-x goal-y) self
    (setf goal-x x goal-y y)))

(defmethod walk-to-thing ((self thing) (destination thing))
  (multiple-value-bind (x y) (center-point destination)
    (walk-toward self x y)))

(defmethod can-see-point-p ((self thing) x y)
  (when *quadtree*
    (block colliding
      (multiple-value-bind (x0 y0) (center-point self)
	(let ((d (/ (distance x0 y0 x y) 100))
	      (w 0)
	      (h (find-heading x0 y0 x y)))
	  (dotimes (n 100)
	    (incf w d)
	    (multiple-value-bind (x1 y1)
		(step-toward-heading self h w)
	      (let* ((vtop (- y1 1))
		     (vleft (- x1 1))
		     (vright (+ vleft 2))
		     (vbottom (+ vtop 2)))
		(flet ((check (object)
			 (when (opaque-p object)
			   (return-from colliding nil))))
		  (prog1 t
		    (xelf::quadtree-map-collisions *quadtree*
						   (cfloat vtop)
						   (cfloat vleft)
						   (cfloat vright)
						   (cfloat vbottom)
						   #'check))))))
	  (return-from colliding t))))))
  
(defmethod can-see-thing-p ((self thing) (other thing))
  (multiple-value-bind (x y) (center-point other)
    (can-see-point-p self x y)))

(defmethod stop-walking ((self thing))
  (with-slots (goal-x goal-y) self
    (setf goal-x nil goal-y nil)))

(defmethod path-heading ((self thing))
  (with-slots (x y goal-x goal-y waypoints) self
    (when (and goal-x goal-y)
      (if (< 4 (distance x y goal-x goal-y))
	  ;; keep walking 
	  (find-heading x y goal-x goal-y)
	  (setf goal-x nil goal-y nil)))))

(defmethod will-obstruct-p ((this thing) (that thing))
  (obstacle-p this))

(defmethod volatile-p ((thing thing))
  (plusp (volatile-clock thing)))

(defmethod opaque-p ((thing thing)) nil)

(defmethod nearest-agent ((thing thing))
  (if (not (co-op-p))
      (agent)
      (let ((h (agent))
	    (s (sidekick)))
	(if (< (distance-between thing h)
	       (distance-between thing s))
	    h
	    s))))

(defmethod update :before ((thing thing))
  (save-location thing)
  (when (plusp (volatile-clock thing))
    (decf (volatile-clock thing))))

(defmethod mark-volatile ((thing thing))
  (setf (volatile-clock thing) *volatile-period*))

(defmethod update :around ((thing thing))
  (unless (or *paused*
	      (game-over-p)
	      (terminal-showing-p))
    (call-next-method)))

(defmethod play-sound ((thing thing) sound)
  (play-sample sound))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod draw ((self thing))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 :window-x (+ (shake) (units -1))
				 :window-y (+ (shake) (units -1))
				 ;; apply shading
				 :vertex-color color
				 :blend :alpha))))

(defmethod impel ((thing thing) direction)
  (setf (direction thing) direction)
  (setf (heading thing) (direction-heading direction)))

;;; Invisible tool icons

(defclass icon (thing)
  ((sample :initform nil :accessor sample :initarg :sample)
   (timer :initform 10 :accessor timer :initarg :timer)
   (height :initform (units 0.2))
   (width :initform (units 0.2))))

(defmethod triggered-p ((icon icon))
  (gethash (uuid icon) *triggers*))

(defmethod (setf triggered-p) (value (icon icon))
  (setf (gethash (uuid icon) *triggers*) value))

(defmethod initialize-instance :after ((icon icon) &key)
  (mark-volatile icon))

(defmethod draw ((icon icon)) nil)

(defmethod trigger ((icon icon))
  (when (sample icon)
    (play-sample (sample icon))))

(defmethod trigger :around ((icon icon))
  (unless (triggered-p icon)
    (call-next-method)
    (setf (triggered-p icon) t)))

;; (defmethod xelf::bridge-revive :after ((thing thing) old-thing)
;;   (when (typep old-thing (find-class 'node))
;;     (message "hi")
;;     (setf (triggered-p thing)
;; 	  (triggered-p old-thing))))

(defmethod update :after ((icon icon))
  (trigger icon)
  (when (plusp (timer icon))
    (decf (timer icon)))
  (when (zerop (timer icon))
    (destroy icon)))

(setf xelf::*ignored-slots* (adjoin 'inv8r::triggered-p xelf::*ignored-slots*))

(defclass death-icon (icon)
  ((sample :initform "analog-death.wav")))

(defmethod trigger :after ((death-icon death-icon))
  (multiple-value-bind (x y) (center-point death-icon)
    (shake-screen 20)
    (make-sparks x y)))

(defclass level-icon (icon)
  ((sample :initform "powerup.wav")))

(defmethod update :around ((death-icon death-icon))
  (trigger death-icon))

(defclass spark-icon (icon)
  ((sample :initform (random-choose *bounce-sounds*))))

(defmethod trigger :after ((spark-icon spark-icon))
  (with-slots (x y) spark-icon
    (make-sparks-* x y)))

;;; Player Bullets

(defclass bullet (thing)
  ((radius :initform 7 :initarg :radius :accessor radius)
   (speed :initform 14 :initarg :speed)))

(defparameter *gamma* 3.5)

(defmethod collide :around ((bullet bullet) (thing thing))
  (multiple-value-bind (x y) (center-point bullet)
    (if (colliding-with-rectangle-p thing
				      (- y *gamma*)
				      (- x *gamma*)
				      (* 2 *gamma*)
				      (* 2 *gamma*))
	(call-next-method)
	;; check interpolated position
	(when (numberp (slot-value bullet 'last-x))
	  (with-slots (last-x last-y) bullet
	    (multiple-value-bind (x0 y0)
		(values (/ (+ x (- last-x *gamma*)) 2)
			(/ (+ y (- last-y *gamma*)) 2))
	      (when (colliding-with-rectangle-p thing
						(- y0 *gamma*)
						(- x0 *gamma*)
						(* 2 *gamma*)
						(* 2 *gamma*))
		(call-next-method))))))))

(defmethod find-image ((bullet bullet))
  (random-choose '("paddle-1.png" "paddle-2.png" "paddle-3.png")))

(defmethod draw ((bullet bullet))
  (with-slots (x y width height heading) bullet
    (draw-textured-rectangle-*
     x y 0 width height
     (find-texture (find-image bullet))
     :window-x (+ (shake) (units -1))
     :window-y (+ (shake) (units -1))
     :angle (+ 90 (heading-degrees heading)))))
     
(defmethod update ((bullet bullet))
  (with-slots (speed) bullet 
    (forward bullet speed)))

(defmethod initialize-instance :after ((bullet bullet) &key)
  (with-slots (radius) bullet
    (resize bullet (* 2 radius) (* 2 radius))))

;;; Sparkle explosion cloud fx

(defclass spark (thing)
  ((clock :initform 12 :accessor clock :initarg :clock)
   (glow-p :initform nil :accessor glow-p)
   (direction :initform (xelf::random-direction))
   (collision-type :initform :passive)))

(defclass glow (thing)
  ((clock :initform 20 :accessor clock :initarg :clock)
   (collision-type :initform :passive)))

(defmethod draw ((glow glow))
  (with-slots (x y clock) glow
    (draw-textured-rectangle-*
     (- x 100 (random 15)) (- y 100 (random 15)) 0 200 200 (find-texture "hoop.png")
     :window-x (shake)
     :window-y (shake)
     :blend :additive
     :vertex-color (list 100 100 255)))
  (set-blending-mode :alpha))
  
(defmethod update ((self glow))
  (with-slots (clock) self
    (when (plusp clock)
      (decf clock))
    (when (zerop clock)
      (destroy self))))

(defmethod draw ((spark spark))
  (set-blending-mode :additive)
  (with-slots (x y clock) spark
    (dotimes (n 2)
      (let ((z (+ 2 (random 4))))
	(draw-textured-rectangle-*
	 (+ x (random 20))
	 (+ y (random 20))
	 0 20 20
	 (find-texture (random-choose *rez-lights*))
	 :blend :additive
	 :opacity 0.95)))
  (set-blending-mode :alpha)))

(defmethod draw :around ((spark spark))
  (when (not (game-over-p))
    (call-next-method)))

(defmethod draw :around ((glow glow))
  (when (not (game-over-p))
    (call-next-method)))

(defmethod update ((self spark))
  (move-toward self (direction self) (+ 2 (random 4)))
  (with-slots (clock glow-p) self
    (when (plusp clock)
      (decf clock))
    (when (not glow-p)
      (setf glow-p t)
      (multiple-value-bind (cx cy) (center-point self)
	(add-node (current-buffer) (make-instance 'glow) cx cy)))
    (when (zerop clock)
      (destroy self))))

(defun make-sparks-* (x y &optional color)
  (dotimes (z 3)
    (add-node (current-buffer)
	      (make-instance 'spark)
	      (+ x -15 (random 30)) (+ y -15 (random 30)))))

(defun make-sparks (x y)
  (add-node (current-buffer) (make-instance 'spark-icon) x y))

;;; The player, a secret agent

(defparameter *powerup-duration* 900)

(defparameter *powerups* '(:bouncy-bullets :invincible :rapid-fire :bouncy-bullets :rapid-fire))

(defun random-item () (make-instance (random-choose '(oxygen-tank powerup powerup))))

(defclass agent (thing)
  ((deadp :initform nil :accessor deadp)
   (powerup :initform nil :accessor powerup)
   (powerup-clock :initform 0 :accessor powerup-clock)
   (color :initform "hot pink" :accessor color :initarg color)
   (data :initform nil :accessor data :initarg :data)
   (oxygen :initform *maximum-oxygen* :accessor oxygen)
   (item :initform nil :accessor item)
   (ready-p :initform t :accessor ready-p)
   (direction :initform :up :accessor direction)
   (kick-direction :initform :up :accessor kick-direction)
   (walk-clock :initform 0 :accessor walk-clock)
   (step-clock :initform 0 :accessor step-clock)
   (kick-clock :initform 0 :accessor kick-clock)
   (ready-clock :initform 0 :accessor ready-clock)
   (running-clock :initform 0 :accessor running-clock)
   (respiration-rate :initform *normal-respiration-rate* :accessor respiration-rate)
   (respiration-clock :initform 0 :accessor respiration-clock)
   (pulse-rate :initform *normal-pulse-rate* :accessor pulse-rate)
   (pulse-sound :initform "pulse-low.wav")
   (pulse-clock :initform 0 :accessor pulse-clock)
   (breaths :initform 0)
   (image :initform "agent.png")
   ;; netplay
   (input-direction :initform nil :accessor input-direction)
   (input-kicking-p :initform nil :accessor input-kicking-p)
   (player-id :initform 1 :accessor player-id)))

(defclass hero (agent) ())

(defclass sidekick (agent)
  ((player-id :initform 2)
   (color :initform "yellow green")))

(defmethod update :before ((agent agent))
  (with-slots (shield-clock) agent
    (when (plusp shield-clock)
      (decf shield-clock))))

(defclass oxygen-tank (thing)
  ((image :initform "oxygen-tank.png")))

(defmethod initialize-instance :after ((oxygen-tank oxygen-tank) &key)
  (resize oxygen-tank 18 18))

(defmethod collide ((oxygen-tank oxygen-tank) (agent agent))
  (play-icon oxygen-tank "powerdown.wav")
  (fill-oxygen agent)
  (destroy oxygen-tank))

(defclass powerup (thing)
  ((image :initform "mtron-1.png")
   (powerup :initform (random-choose *powerups*))))

(defmethod initialize-instance :after ((powerup powerup) &key)
  (resize powerup 15 15))

(defmethod update :after ((powerup powerup))
  (setf (slot-value powerup 'image) (random-choose '("mtron-1.png" "mtron-2.png" "mtron-3.png"))))

(defmethod collide ((powerup powerup) (agent agent))
  (play-icon powerup "go.wav")
  (setf (powerup agent) (slot-value powerup 'powerup))
  (setf (powerup-clock agent) *powerup-duration*)
  (destroy powerup))

(defun core-pulse (oxygen)
  (let ((index (truncate (* oxygen (/ (length *pulse-sounds*) *maximum-oxygen*)))))
    (or (nth index *pulse-sounds*) "pulse-6.wav")))

(defparameter *pulse-durations* '(100 95 90 85 80 75 70 65 60 55 50 45 40 35 30 25 20 15))

(defun pulse-duration (oxygen)
  (let ((index (truncate (* oxygen (/ (length *pulse-durations*) *maximum-oxygen*)))))
    (or (nth index *pulse-durations*) 100)))

(defmethod find-pulse-sound ((agent agent))
  (with-slots (oxygen) agent
    (core-pulse (- *maximum-oxygen* oxygen))))

(defmethod find-pulse-duration ((agent agent))
  (pulse-duration (- *maximum-oxygen* (oxygen agent))))

(defmethod fill-oxygen ((agent agent))
  (setf (oxygen agent) *maximum-oxygen*))

(defparameter *low-oxygen* 20)

(defmethod low-oxygen-p ((agent agent))
  (<= (oxygen agent) *low-oxygen*))

(defmethod out-of-oxygen-p ((agent agent))
  (not (plusp (oxygen agent))))

(defmethod use-oxygen ((agent agent) &optional (points 1))
  (setf (oxygen agent)
	(max 0 (- (oxygen agent) points))))

(defmethod use-oxygen :around ((agent agent) &optional (points 1))
  (unless (= *boss-level* *level*)
    (call-next-method)))

(defparameter *traditional-agent-colors* '("gold" "olive drab" "RoyalBlue3" "dark orchid"))

(defparameter *agent-size* 26)

(defmethod initialize-instance :after ((agent agent) &key)
  (resize agent *agent-size* *agent-size*))

(defparameter *agent-colors* '("gold" "olive drab" "RoyalBlue3" "dark orchid"))

(defparameter *epsilon* 4)
(defmethod collide :around ((agent agent) (thing thing))
  (multiple-value-bind (x y) (center-point agent)
    (when (colliding-with-rectangle-p thing
				      (- y *epsilon*)
				      (- x *epsilon*)
				      (* 2 *epsilon*)
				      (* 2 *epsilon*))
      (call-next-method))))

;;; Drawing the agent onscreen and animating his feet

(defparameter *agent-step-frames* 4)
(defparameter *agent-empty-color* "white")
(defparameter *agent-speed* 8)
(defparameter *agent-reload-frames* 7)
(defparameter *walk-interval* 24)

(defmethod animate-walk ((agent agent))
  (with-slots (walk-clock step-clock) agent
    ;; only when moving
    (when (plusp step-clock)
      (when (plusp walk-clock)
	(decf walk-clock))
      (when (zerop walk-clock)
	(setf walk-clock *walk-interval*)))))

(defparameter *walking-right* 
  '("agent-right.png"
    "agent-right-lstep.png"
    "agent-right.png"
    "agent-right-rstep.png"))

(defparameter *walking-up* 
  '("agent-up.png"
    "agent-up-lstep.png"
    "agent-up.png"
    "agent-up-rstep.png"))

(defun agent-image (direction clock)
  (let ((frames
	  (or 
	   (cond
	     ((member direction '(:up :down :upright :downleft))
	      *walking-up*)
	     ((member direction '(:left :right :upleft :downright))
	      *walking-right*))
	   *walking-up*)))
    (or (nth (truncate (* clock (/ 4 *walk-interval*)))
	     frames)
	(if (or (eq direction :left) (eq direction :right))
	    "agent-right.png"
	    "agent-up.png"))))

(defmethod serve-location ((agent agent))
  (with-slots (direction) agent
    (multiple-value-bind (cx cy) (center-point agent)
      (multiple-value-bind (tx ty) 
	  (step-in-direction cx cy direction (units 0.1))
	(values (+ tx (units 1) (+ ty (units 1))))))))

(defmethod draw ((agent agent))
  (with-slots (direction walk-clock shield-clock) agent
    (let ((image 
	    (if (not (deadp agent))
		(or
		 (agent-image direction walk-clock) 
		 "agent-up.png")
		"skull.png")))
      (with-slots (x y width height color) agent
	(draw-textured-rectangle x y 0 width height
				 (find-texture image) 
				 :vertex-color color)))))

(defmethod serve ((agent agent))
  (with-slots (kick-direction height width powerup) agent
    (multiple-value-bind (x y) (center-point agent)
      (let ((bullet (make-instance
		     (if (eq :bouncy-bullets powerup) 'player-bouncy-bullet 'bullet)))
	    (x0 (- x (/ width 2)))
	    (y0 (- y (/ height 2))))
	(add-node (current-buffer) bullet x0 y0)
	(play-icon agent (fire-sound bullet))
	(impel bullet kick-direction)))))

;;; Cool vintage footstep and kick sounds

(defresource "left-foot.wav" :volume 70)
(defresource "right-foot.wav" :volume 70)

(defmethod footstep-sound ((agent agent))
  (case (mod xelf::*updates* 8)
    (0 "left-foot.wav")
    (3 "left-foot.wav")))

(defparameter *footstep-sound-range* 300)

(defmethod make-footstep-sounds ((agent agent))
  (let ((sound (footstep-sound agent)))
    (when sound 
      (play-sound agent sound))))

(defresource "kick.wav" :volume 23)
(defresource "serve.wav" :volume 23)

(defparameter *kick-sound* "kick.wav")

(defparameter *joystick-enabled* nil)

(defun any-joystick () *any-joystick*)

(defmethod stick-heading ((agent agent))
  (when (or (when (any-joystick) 0) *player-1-joystick*)
    (when (left-analog-stick-pressed-p (or (when (any-joystick) 0) *player-1-joystick*))
      (left-analog-stick-heading (or (when (any-joystick) 0) *player-1-joystick*)))))

(defmethod aim-heading ((agent agent))
  (when (or (when (any-joystick) 0) *player-1-joystick*)
    (when (right-analog-stick-pressed-p (or (when (any-joystick) 0) *player-1-joystick*))
      (right-analog-stick-heading (or (when (any-joystick) 0) *player-1-joystick*)))))

(defmethod aim-direction ((agent agent))
  (let ((h (aim-heading agent)))
    (when h
      (or (heading-direction h)
          :left))))

(defmethod stick-direction ((agent agent))
  (let ((h (stick-heading agent)))
    (when h
      (or (heading-direction h)
	  :left))))

(defmethod movement-direction ((agent agent))
  (stick-direction agent))

(defmethod collide ((agent agent) thing)
  (when (obstacle-p thing)
    (restore-location agent)))

(defmethod die ((agent agent))
  (with-slots (x y) agent
    (add-node (arena) (make-instance 'death-icon) x y))
  (change-image agent "skull.png")
  (lose-life)
  (start-retry-clock)
  (setf (deadp agent) t))

(defmethod die :around ((agent agent))
  (unless (or (deadp agent)
	      (eq :invincible (powerup agent))
	      (plusp (shield-clock agent)))
    (call-next-method)))

(defmethod die :around ((sidekick sidekick))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod kick ((agent agent))
  (with-slots (kick-clock) agent
    (when (zerop kick-clock)
      (serve agent)
      ;;(play-sound agent (random-bip-sound))
      (setf kick-clock (if (eq :rapid-fire (powerup agent))
			   16
			   *agent-reload-frames*)))))

(defmethod kick :around ((agent agent))
  (unless (deadp agent) (call-next-method)))

(defmethod paint ((agent agent) color)
  (setf (color agent) color))

(defmethod kicking-p ((agent agent))
  (holding-fire-p agent))

(defmethod play-icon ((thing thing) sample)
  (with-slots (x y) thing
    (add-node (current-buffer) (make-instance 'icon :sample sample) x y)))

(defmethod update ((agent agent))
  (when (zerop (mod xelf:*updates* (by-level 50 40 40 40 40 30 30 30 30 30 30 25 25 25 25 25 20 20 20 19)))
    (use-oxygen agent))
  (when (out-of-oxygen-p agent)
    (die agent))
  (with-slots (powerup powerup-clock step-clock walk-clock respiration-rate pulse-rate kick-direction kick-clock respiration-clock pulse-clock breaths pulse-sound image) agent
    (when (zerop powerup-clock)
      (setf powerup nil))
    (when (plusp powerup-clock)
      (decf powerup-clock))
    ;; update respiration 
    (if (low-oxygen-p agent)
	(setf respiration-rate 18)
	(if (< (oxygen agent) 60)
	    (setf respiration-rate 14)
	    (setf respiration-rate 10)))
    (when (plusp respiration-clock)
      (decf respiration-clock))
    (when (zerop respiration-clock)
      (setf respiration-clock (truncate (seconds->frames (/ 60 respiration-rate))))
      (incf breaths)
      (play-icon agent (nth (mod breaths (length *respiration-sounds*))
			*respiration-sounds*)))
    ;; update pulse 
    (when (plusp pulse-clock)
      (decf pulse-clock))
    (when (zerop pulse-clock)
      (setf pulse-clock (truncate (find-pulse-duration agent)))
      (play-icon agent (find-pulse-sound agent)))
    ;; update animation and movement
    (when (plusp step-clock)
      (decf step-clock))
    ;; find out what direction the AI or human wants to go
    (let ((direction (movement-direction agent))
	  (kick-button (kicking-p agent)))
	(when direction 
	  ;; move in the movement direction
	  (move-toward agent direction (/ *agent-speed* 2))
	  (setf (direction agent) direction))
	(if direction
	    ;; controller is pushing in a direction
	    (when (zerop step-clock)
	    ;; don't animate on every frame
	      (setf step-clock *agent-step-frames*)
	      ;; possibly make footstep sounds
	      (make-footstep-sounds agent))
	    ;; not pushing. allow movement immediately
	    (setf step-clock 0 walk-clock 0))
	;; update animation
	(animate-walk agent)
      ;; delay between kicks
      (when (plusp kick-clock)
	(decf kick-clock))
      ;; ready to kick?
      (when (zerop kick-clock)
	(when kick-button
	  ;; yes, do it
          (setf kick-direction (aim-direction agent))
	  (kick agent))))))

(defmethod holding-fire-p ((agent agent))
  (or (holding-shift-p)
      (when *player-1-joystick* (right-analog-stick-pressed-p *player-1-joystick*))))
      ;; (when *player-1-joystick* (holding-button-p *player-1-joystick*))

(defmethod holding-direction-p ((agent agent))
  (or (arrow-keys-direction)
      (left-analog-stick-pressed-p)))

(defmethod update :around ((agent agent))
  (when (or (holding-direction-p agent)
	    (holding-fire-p agent))
    (hide-terminal))
  (when (deadp agent)
    (resize agent 23 23)
    (setf (slot-value agent 'color) (random-choose '("magenta" "yellow"))))
  (unless (or (terminal-showing-p)
	      *paused*
	      (and (game-over-p)
		   (deadp agent)))
    (call-next-method)))

;;; Indestructable deathwalls

(defun random-wall-color () (random-choose '("dark orange" "dark red" "royal blue" "medium violet red" "yellow")))
(defparameter *wall-color* (random-wall-color))

(defclass wall (thing)
  ((color :initform *wall-color*)
   (obstacle-p :initform t)))

(defmethod draw ((wall wall))
  (with-slots (x y width height color) wall
    (draw-box x y width height :color color)))

(defmethod opaque-p ((wall wall)) t)

(defparameter *brick-width* (units 1.8))
(defparameter *brick-height* (units 1.2))

(defmethod leak-oxygen ((agent agent))
  (play-sample "yelp.wav")
  (multiple-value-bind (x y) (center-point agent)
    (make-sparks x y))
  (use-oxygen agent (* 2 (by-level 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 15))))

(defmethod leak-oxygen :around ((agent agent))
  (unless (or (deadp agent)
	      (eq :invincible (powerup agent))
	      (plusp (shield-clock agent)))
    (call-next-method)))

(defmethod collide ((wall wall) (agent agent))
  (if *sweep*
      (destroy wall)
      (progn (leak-oxygen agent)
	     (restore-location agent))))

(defmethod destroy :before ((bullet bullet))
  (unless (clientp (arena))
    (multiple-value-bind (x y) (center-point bullet)
      (make-sparks x y))))

(defmethod collide ((wall wall) (bullet bullet))
  (destroy bullet))

(defmethod fire-sound ((bullet bullet))
  (random-bip-sound))

;;; Enemy bullet

(defclass enemy-bullet (bullet)
  ((speed :initform (by-level 2.5 2.7 3.1 3.3 3.6 3.8 4 4.3 4.6 4.9 5.1 5.1 5.3 5.3 5.7 5.7 6.1 6.1 6.5))))

(defmethod find-image ((enemy-bullet enemy-bullet))
  (random-choose '("enemy-bullet-1.png" "enemy-bullet-2.png" "enemy-bullet-3.png")))

(defmethod collide ((enemy-bullet enemy-bullet) (agent agent))
  (destroy enemy-bullet)
  (die agent))

;;; Enemy trail

(defclass trail (thing)
  ((clock :initform (truncate (* 1.3 (by-level 100 100 110 110 120 120 130 130 140 140 150 150 160 160 170 170 180 180 190 190 200 200 210 210 220 220 230 230 240 250 260 270 280))))
   (collision-type :initform :passive)))


(defmethod update ((self trail))
  (with-slots (clock) self
    (when (plusp clock)
      (decf clock))
    (when (zerop clock)
      (destroy self))))

(defmethod find-image ((trail trail))
  (random-choose '("trail-1.png" "trail-2.png")))

(defmethod collide ((trail trail) (agent agent))
  (die agent))

(defmethod draw ((trail trail))
  (with-slots (x y width height heading) trail
    (draw-textured-rectangle-*
     x y 0 width height
     (find-texture (find-image trail))
     :window-x (+ (shake) (units -1))
     :window-y (+ (shake) (units -1))
     :angle (+ 90 (heading-degrees heading)))))

(defmethod initialize-instance :after ((trail trail) &key)
  (mark-volatile trail)
  (resize trail 12 12))

;;; Bouncy bullet

(defclass bouncy-bullet (enemy-bullet)
  ((timer :initform (by-level 140 150 160 170 180 190 200 210 220 230 240 250 260 270 280 290))))

(defmethod find-image ((bouncy-bullet bouncy-bullet))
  (random-choose '("bouncy-bullet-1.png" "bouncy-bullet-2.png" "bouncy-bullet-3.png")))

(defmethod collide ((wall wall) (bouncy-bullet bouncy-bullet))
  (restore-location bouncy-bullet)
  (mark-volatile bouncy-bullet)
  (impel bouncy-bullet
	 (random-choose (list (xelf::left-turn (xelf::left-turn (slot-value bouncy-bullet 'direction)))
			      (xelf::right-turn (xelf::right-turn (slot-value bouncy-bullet 'direction)))))))

(defmethod update :after ((bouncy-bullet bouncy-bullet))
  (with-slots (timer) bouncy-bullet
    (when (plusp timer)
      (decf timer))
    (when (zerop timer) 
      (destroy bouncy-bullet))))

(defclass player-bouncy-bullet (bouncy-bullet)
  ((speed :initform 6)))

(defmethod collide ((player-bouncy-bullet player-bouncy-bullet) (agent agent)) nil)
(defmethod collide ((agent agent) (player-bouncy-bullet player-bouncy-bullet)) nil)

(defmethod find-image ((player-bouncy-bullet player-bouncy-bullet))
  (random-choose '("paddle-1.png" "paddle-2.png" "paddle-3.png")))

(defmethod fire-sound ((player-bouncy-bullet player-bouncy-bullet))
  "serve.wav")

;;; Droid

(defclass droid (thing)
  ((image :initform "humanoid.png")
   (direction :initform (xelf::random-direction))))

(defmethod fire-sound ((droid droid))
  "grow.wav")

(defmethod collide ((player-bouncy-bullet player-bouncy-bullet) (droid droid))
  (destroy droid)
  (destroy player-bouncy-bullet))

(defmethod initialize-instance :after ((droid droid) &key)
  (resize droid 22 22))

(defmethod update ((droid droid))
  (if (< (distance-between droid (nearest-agent droid))
	 (by-level 240 250 260 270 280 290 300 310 320 330 340 350 360 370 370 380 390))
      (progn (move droid (heading-between droid (nearest-agent droid)) (by-level 0.5 0.6 1 1.2 1.3 1.4 1.6 1.9))
	     (percent-of-time (by-level 1 1.1 1.1 1.1 1.2 1.2 1.3 1.3 1.3 1.4 1.4 1.5 1.5 1.6 1.7 1.7 1.7 1.8 1.9) (attack droid)))
      (move droid (direction-heading (slot-value droid 'direction))
	    (by-level 0.4 0.5 0.6 0.7 0.8 0.9 1))))

(defmethod destroy :before ((droid droid))
  (when (and
	 (not *sweep*)
	 (typep (current-buffer) (find-class 'arena)))
    (shake-screen)
    (percent-of-time 20
      (with-slots (x y) droid
	(add-node (current-buffer) (random-item) x y))))
  (when (and (not *sweep*)
	     (not (clientp (arena)))
	     (= 1 (length (find-instances (current-buffer) 'droid))))
    ;; last one!
    (play-sample "powerup.wav")
    (play-sample "explode.wav")))

(defmethod collide ((droid droid) (wall wall))
  (if *sweep*
      (destroy droid)
      (with-slots (direction) droid
	(restore-location droid)
	(mark-volatile droid)
	(setf direction (xelf::random-direction)))))

(defmethod collide ((droid droid) (agent agent))
  (destroy droid)
  (die agent))

(defmethod collide ((droid droid) (bullet bullet))
  (destroy droid)
  (destroy bullet))

(defmethod collide ((droid droid) (enemy-bullet enemy-bullet)) nil)

(defun firing-direction (a b)
  (multiple-value-bind (x y) (center-point a)
    (multiple-value-bind (x0 y0) (center-point b)
      (setf x (truncate (/ x (units 3))))
      (setf y (truncate (/ y (units 3))))
      (setf x0 (truncate (/ x0 (units 3))))
      (setf y0 (truncate (/ y0 (units 3))))
      (find-direction x y x0 y0))))

(defmethod attack ((droid droid))
  (let ((dir (firing-direction droid (nearest-agent droid))))
    (multiple-value-bind (x y) (center-point droid)
      (let* ((bullet (make-instance 'enemy-bullet))
	    (x0 (- x (/ (slot-value bullet 'width) 2)))
	    (y0 (- y (/ (slot-value bullet 'height) 2))))
	(add-node (current-buffer) bullet x0 y0)
	(play-icon droid (fire-sound bullet))
	(impel bullet dir)))))

(defmethod attack :around ((droid droid))
  (unless (clientp (current-buffer))
    (call-next-method)))

(defmethod fire-sound ((enemy-bullet enemy-bullet))
  "muon-fire.wav")

(defclass biclops (droid)
  ((image :initform "biclops.png")
   (direction :initform (random-choose '(:up :down)))))

(defmethod update ((biclops biclops))
  (when (< (distance-between biclops (nearest-agent biclops))
	   (by-level 240 290 340 380 420 490 520 580))
    (percent-of-time (by-level 1.7 1.8 1.9 2 2 2.1 2.2 2.3) (attack biclops)))
  (move biclops (direction-heading (slot-value biclops 'direction)) (by-level 0.2 0.4 0.6 0.8 1 1.2 1.4 1.7 2 2.1)))

(defmethod collide ((biclops biclops) (wall wall))
  (if *sweep*
      (destroy biclops)
  (with-slots (direction) biclops
    (restore-location biclops)
    (mark-volatile biclops)
    (setf direction (random-choose '(:upright :upleft :downright :downleft :up :down :left :right))))))

(defparameter *drone-image* "agent-run-1.png")
(defparameter *drone-running-images* (image-set "agent-run" 3))
(defparameter *mismunch-images* (image-set "mismunch" 6))

(defclass tracer (droid)
  ((image :initform "tracer.png")
   (traveled-distance :initform 0 :accessor traveled-distance)
   (hit-points :initform 2 :accessor hit-points)
   (direction :initform (if (evenp *level*)
			    (random-choose '(:up :down :left :right))
			    (random-choose '(:upright :downleft :upleft :downright))))))

(defmethod update ((tracer tracer))
  (with-slots (direction heading traveled-distance x y) tracer
    (let ((delta (by-level 0.2 0.4 0.6 0.8 1 1.2 1.4 1.7 2 2.1 2.15 2.15 2.2 2.2 2.25 2.25 2.3 2.3 2.35)))
      (incf traveled-distance delta)
      (when (< 18 traveled-distance)
	(setf traveled-distance 0)
	(add-node (current-buffer) (make-instance 'trail :heading (direction-heading direction)) (+ x 4) (+ y 4)))
      (setf heading (direction-heading direction))
      (move tracer heading delta))))

(defmethod collide ((bullet bullet) (tracer tracer)) nil)
(defmethod collide ((tracer tracer) (bullet bullet))
  (play-icon tracer (random-choose *blaagh-sounds*))
  (decf (hit-points tracer))
  (destroy bullet)
  (unless (plusp (hit-points tracer))
    (destroy tracer)))

(defmethod collide ((enemy-bullet enemy-bullet) (tracer tracer)) nil)
(defmethod collide ((tracer tracer) (enemy-bullet enemy-bullet)) nil)

(defmethod draw ((tracer tracer))
  (with-slots (x y width height heading) tracer
    (draw-textured-rectangle-*
     x y 0 width height
     (find-texture "tracer.png")
     :window-x (+ (shake) (units -1))
     :window-y (+ (shake) (units -1))
     :angle (+ 90 (heading-degrees heading)))))

(defmethod collide ((tracer tracer) (wall wall))
  (if *sweep*
      (destroy tracer)
      (with-slots (direction) tracer
	(restore-location tracer)
	(mark-volatile tracer)
	(setf direction (random-choose (list (xelf::left-turn (xelf::left-turn direction))
					     (xelf::right-turn (xelf::right-turn direction))))))))
  
(defmethod initialize-instance :after ((tracer tracer) &key)
  (resize tracer 20 20))

(defclass mismunch (droid)
  ((image :initform (random-choose *mismunch-images*))
   (direction :initform (random-choose '(:up :down :left :right)))))

(defmethod initialize-instance :after ((mismunch mismunch) &key)
  (resize mismunch 20 20))

(defmethod update ((mismunch mismunch))
  (percent-of-time 8
    (setf (slot-value mismunch 'image) (random-choose *mismunch-images*)))
  (percent-of-time 2 (play-sample "munch1.wav"))
  (move mismunch (direction-heading (slot-value mismunch 'direction))
	(+
	 (if (< (distance-between mismunch (nearest-agent mismunch))
		(by-level 150 150 160 160 170 170 180 180 190 190 200 210 220 230 240 250 260 270 280 290 300))
	     (by-level 1 1 1 1 1 1.2 1.2 1.2 1.3 1.3 1.4 1.4 1.7 1.7 1.7 1.7 1.9 1.9 2)
	     0)
	 (by-level 1.7 1.8 1.8 1.9 1.9 2 2 2 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.9 3 3.1 3.2))))
  
(defmethod collide ((mismunch mismunch) (wall wall))
  (if *sweep*
      (destroy mismunch)
      (with-slots (direction) mismunch
	(restore-location mismunch)
	(mark-volatile mismunch)
	(setf direction (opposite-direction direction)))))

;;; Spores

(defclass spore (mismunch)
  ((image :initform (random-choose *sprout-images*))
   (hp :initform 1)
   (direction :initform (random-choose '(:up :down :left :right :upleft :upright :downleft :downright)))))

(defparameter *max-spores* 75)

(defvar *spore-instances* 0)

(defmethod initialize-instance :after ((spore spore) &key)
  (incf *spore-instances*)
  (resize spore 25 25))

(defmethod destroy :before ((spore spore))
  (decf *spore-instances*))

(defmethod update ((spore spore))
  (with-slots (hp) spore
    (if (zerop hp)
        (destroy spore)
        (progn (setf (slot-value spore 'image) (nth (1- hp) *sprout-images*))
               (percent-of-time 0.12
                 (setf (slot-value spore 'direction) (xelf::random-direction))
                 (setf hp (min 6 (1+ hp))))
               (percent-of-time 0.12
                 (unless (> *spore-instances* *max-spores*)
                   (insert (make-instance 'spore) (slot-value spore 'x) (slot-value spore 'y))))
               (move spore (direction-heading (slot-value spore 'direction))
                     (* 0.24
	                (+
	                 (if (< (distance-between spore (nearest-agent spore))
		                (by-level 150 150 160 160 170 170 180 180 190 190 200 210 220 230 240 250 260 270 280 290 300))
	                     (by-level 1 1 1 1 1 1.2 1.2 1.2 1.3 1.3 1.4 1.4 1.7 1.7 1.7 1.7 1.9 1.9 2)
	                     0)
	                 (by-level 1.7 1.8 1.8 1.9 1.9 2 2 2 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.9 3 3.1 3.2))))))))

(defmethod collide ((spore spore) (bullet bullet))
  (play-sample "munch1.wav")
  (decf (slot-value spore 'hp))
  (destroy bullet))
        
(defmethod collide ((spore spore) (wall wall))
  (if *sweep*
      (destroy spore)
      (with-slots (direction) spore
	(restore-location spore)
	(mark-volatile spore)
	(setf direction (opposite-direction direction)))))

(defclass monitor (droid)
  ((image :initform "scanner.png")
   (direction :initform (xelf::random-direction))))

(defmethod initialize-instance :after ((monitor monitor) &key)
  (resize monitor 25 25))

(defmethod update ((monitor monitor))
  (when (< (distance-between monitor (nearest-agent monitor))
	   (by-level 240 290 340 380 420 490 520 580))
    (percent-of-time (by-level 1.8 1.8 1.9 1.9 1.9 1.9 2 2 2 2 2.1 2.1 2.2 2.2 2.3) (attack monitor))))

(defmethod attack ((monitor monitor))
  (multiple-value-bind (x y) (center-point monitor)
    (let* ((bullet (make-instance 'bouncy-bullet))
	    (x0 (- x (/ (slot-value bullet 'width) 2)))
	    (y0 (- y (/ (slot-value bullet 'height) 2))))
      (add-node (current-buffer) bullet x0 y0)
      (play-icon monitor (fire-sound bullet))
      (impel bullet (random-choose '(:upleft :upright :downleft :downright))))))

(defmethod fire-sound ((bouncy-bullet bouncy-bullet))
  "geiger2.wav")

;;; Gigaboss

(defparameter *boss-images* (image-set "reactor" 9))

(defclass boss (droid) 
  ((image :initform (random-choose *boss-images*))
   (heading :initform (random (* 2 pi)))
   (hit-points :initform 50 :accessor hit-points)))

(defmethod initialize-instance :after ((boss boss) &key)
  (resize boss 55 55))

(defclass giga-bullet (enemy-bullet)
  ((speed :initform (by-level 2.5 2.7 3.1 3.3 3.6 3.8 4 4.3 4.6 4.9 5.1))
   (radius :initform 3)))

(defmethod fire-sound ((giga-bullet giga-bullet))
  "geiger2.wav")

(defmethod find-image ((giga-bullet giga-bullet))
  (random-choose '("bullet.png" "bullet2.png" "bullet3.png")))

(defmethod collide ((giga-bullet giga-bullet) (agent agent))
  (destroy giga-bullet)
  (die agent))

(defmethod attack ((boss boss))
  (multiple-value-bind (x y) (center-point boss)
    (let* ((bullet (or (percent-of-time 1.7 (make-instance (random-choose '(droid rook biclops))))
		       (make-instance 'giga-bullet)))
	    (x0 (- x (/ (slot-value bullet 'width) 2)))
	    (y0 (- y (/ (slot-value bullet 'height) 2))))
      (add-node (current-buffer) bullet x0 y0)
      (play-icon boss (fire-sound bullet))
      (setf (slot-value bullet 'heading) (random (* 2 pi))))))

(defmethod collide ((bullet bullet) (boss boss)) nil)
(defmethod collide ((giga-bullet giga-bullet) (boss boss)) nil)
(defmethod collide ((boss boss) (giga-bullet giga-bullet)) nil)
(defmethod collide ((bullet bullet) (boss boss)) nil)
(defmethod collide ((boss boss) (bullet bullet))
  (play-icon boss (random-choose *blaagh-sounds*))
  (decf (hit-points boss))
  (destroy bullet)
  (unless (plusp (hit-points boss))
    (destroy boss)))

(defmethod collide ((boss boss) (wall wall))
  (restore-location boss)
  (mark-volatile boss))

(defmethod draw ((boss boss))
  (with-slots (x y width height image) boss
    (draw-box x y width height :color (random-choose '("magenta" "hot pink")))
    (draw-textured-rectangle-* x y 0 width height (find-texture image)
			       :window-x (+ (units -1) (shake))
			       :window-y (+ (units -1) (shake))
			       :vertex-color (random-choose '("yellow" "orange" "white")))))

(defmethod update ((boss boss))
  (incf (heading boss) 0.03)
  (percent-of-time 1 (setf (heading boss) (random (* 2 pi))))
  (forward boss 3)
  (setf (slot-value boss 'image)
	(nth (truncate (mod (/ *updates* 10) (length *boss-images*)))
	     *boss-images*))
  (percent-of-time 15 (attack boss)))

(defmethod collide ((boss boss) (agent agent))
  (die agent))

(defmethod collide ((agent agent) (boss boss))
  (die agent))

;;; AI bot

(defclass drone (droid)
  ((image :initform "agent-run-1.png")
   (hit-points :initform 3 :accessor hit-points)
   (direction :initform (xelf::random-direction))))

(defmethod collide ((enemy-bullet enemy-bullet) (drone drone)) nil)
(defmethod collide ((drone drone) (enemy-bullet enemy-bullet)) nil)

(defmethod collide ((bullet bullet) (drone drone)) nil)
(defmethod collide ((drone drone) (bullet bullet))
  (play-icon drone (random-choose *blaagh-sounds*))
  (decf (hit-points drone))
  (destroy bullet)
  (unless (plusp (hit-points drone))
    (destroy drone)))

(defmethod initialize-instance :after ((drone drone) &key)
  (resize drone 25 25))

(defmethod update ((drone drone))
  (when (zerop (mod *updates* 10))
    (setf (slot-value drone 'image) (random-choose *drone-running-images*)))
  (when (zerop (mod *updates* 60))
    (when (can-see-thing-p drone (nearest-agent drone))
      (walk-to-thing drone (nearest-agent drone))
      (attack drone)))
  (when (path-heading drone)
    (move drone (path-heading drone) 1.32))
  (when (null (goal-x drone))
    (move-toward drone (slot-value drone 'direction)
		 1.2)))

(defmethod collide ((drone drone) (wall wall))
  (restore-location drone)
  (setf (slot-value drone 'direction) (random-choose '(:up :down :left :right)))
  (stop-walking drone))

;;; Binary gate

(defparameter *gates* nil)

(defun gate-level-p (level)
  (and
   (not (= *boss-level* *level*))
   (> level 5)
   (evenp level)
   (random-choose '(t nil))))

(defparameter *gate-open-frames* 120)

(defun gate-flicker () (random-choose (list *wall-color* "cyan" "DodgerBlue")))

(defclass gate (wall)
  ((color :initform (gate-flicker))
   (image :initform "barrier.png")
   (clock :initform 0 :accessor clock))) 

(defmethod open-p ((gate gate))
  (plusp (clock gate)))

(defmethod opaque-p :around ((gate gate))
  (not (open-p gate)))

(defmethod update :before ((gate gate))
  (with-slots (image color clock height width) gate
    (when (> height width)
      (setf image "barrier2.png"))
    (when (<= height width)
      (setf image "barrier.png"))
    (setf color (gate-flicker))
    (when (plusp clock)
      (decf clock)
      (when (zerop clock)
	(play-icon gate "gate-closing-sound.wav")))))

(defmethod collide ((gate gate) (agent agent))
  (die agent))

(defmethod collide :around ((gate gate) (agent agent))
  (unless (open-p gate)
    (call-next-method)))

(defmethod collide :around ((agent agent) (gate gate))
  (unless (open-p gate)
    (call-next-method)))

(defmethod collide ((gate gate) (bullet bullet))
  (play-icon gate "gate-closing-sound2.wav")
  (setf (clock gate) *gate-open-frames*)
  (destroy bullet))

(defmethod collide ((gate gate) (bullet player-bouncy-bullet))
  (play-icon gate "gate-closing-sound2.wav")
  (setf (clock gate) *gate-open-frames*)
  (destroy bullet))

(defmethod collide :around ((gate gate) (bullet bullet))
  (if (typep bullet (find-class 'enemy-bullet))
      (call-next-method)
      (when (not (open-p gate))
	(call-next-method))))

(defmethod collide :around ((gate gate) (bullet player-bouncy-bullet))
  (when (not (open-p gate))
    (call-next-method)))

(defmethod collide ((gate gate) (enemy-bullet enemy-bullet))
  (when (not (open-p gate))
    (destroy enemy-bullet)))
(defmethod collide ((enemy-bullet enemy-bullet) (gate gate)) nil)

(defmethod collide :around ((droid droid) (gate gate))
  (when (not (open-p gate))
    (call-next-method)))

(defmethod collide :around ((gate gate) (droid droid)) 
  (when (not (open-p gate))
    (call-next-method)))

(defmethod draw ((gate gate))
  (with-slots (x y width height color image) gate
    (if (not (open-p gate))
	(draw-textured-rectangle x y 0 width height (find-texture image)
				 :vertex-color color)
	(draw-textured-rectangle-* x y 0 width height (find-texture image)
						       :window-x (+ (shake) (units -1))
						       :window-y (+ (shake) (units -1))
						       :blend :additive
						       :vertex-color '(100 100 255 50)))
    (set-blending-mode :alpha)))

;;; Bombs

(defparameter *bomb-images* '("bomb1.png" "bomb2.png" "bomb3.png" "bomb4.png"))
(defparameter *explosion-images* '("bomb-flash1.png" "bomb-flash2.png" "bomb-flash3.png"))
(defparameter *bomb-frames* 260)

(defun bomb-frames ()
  (by-level 260 260 260 250 250 240 240 230 230 220 220 210 210 200 200 190 190 180 180 170 170 170))

(defun bomb-image (n)
  (nth (mod (truncate (* n (/ (length *bomb-images*) (bomb-frames))))
	    (length *bomb-images*))
       *bomb-images*))

(defun bomb-sound (n)
  (when (not (string= (bomb-image n)
		      (bomb-image (1+ n))))
    "alarm.wav"))

(defclass explosion (thing)
  ((image :initform (random-choose *explosion-images*))
   (height :initform 16)
   (width :initform 16)
   (clock :initform 30)))

(defmethod update ((explosion explosion))
  (mark-volatile explosion)
  (with-slots (clock image) explosion
    (move explosion (random (* 2 pi)) 7)
    (setf image (random-choose *explosion-images*))
    (decf clock)
    (when (zerop clock)
      (destroy explosion))))

(defclass bomb (thing)
  ((image :initform "bomb4.png")
   (clock :initform (bomb-frames))
   (speed :initform (by-level 2 2 2 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3.5 3.5 4 4 4))))

(defmethod update ((bomb bomb))
  (with-slots (image clock heading speed) bomb
    (forward bomb speed)
    (when (plusp clock)
      (decf clock)
      (setf image (bomb-image clock))
      (let ((sound (bomb-sound clock)))
	(when sound (play-icon bomb sound)))
      (when (zerop clock)
	(explode bomb)))))

(defmethod explode ((bomb bomb))
  (multiple-value-bind (x y) (center-point bomb)
    (shake-screen 50)
    (play-icon bomb "bigboom.wav")
    (play-icon bomb "xplod.wav")
    (dotimes (n 5)
      (let ((explosion (make-instance 'explosion)))
	(add-node (current-buffer) explosion)
	(move-to explosion (+ x -32 (random 48)) (+ y -32 (random 48)))))
    (destroy bomb)))

(defmethod destroy :before ((bomb bomb))
  (multiple-value-bind (x y) (center-point bomb)
    (make-sparks x y)))

(defmethod initialize-instance :after ((bomb bomb) &key)
  (resize bomb 16 16))

(defmethod collide ((bomb bomb) (wall wall))
  (explode bomb))

(defmethod collide ((bomb bomb) (agent agent))
  (die agent)
  (explode bomb))

(defmethod collide ((agent agent) (explosion explosion))
  (die agent))

(defmethod collide ((wall wall) (bomb bomb)) nil)

(defmethod collide ((bomb bomb) (gate gate))
  (when (and (not (clientp (current-buffer)))
	     (not (open-p gate)))
    (explode bomb)))

;;; The rook

(defclass rook (drone)
  ((image :initform "rook.png")
   (hit-points :initform 1)))

(defmethod initialize-instance :after ((rook rook) &key)
  (resize rook 18 18))

(defmethod throw-bomb ((rook rook))
  (play-sample "bombs-away.wav")
  (let ((dir (firing-direction rook (nearest-agent rook))))
    (multiple-value-bind (x y) (center-point rook)
      (let* ((bullet (make-instance 'bomb))
	     (x0 (- x (/ (slot-value bullet 'width) 2)))
	     (y0 (- y (/ (slot-value bullet 'height) 2))))
	(add-node (current-buffer) bullet x0 y0)
	(setf (slot-value bullet 'heading) (heading-between rook (nearest-agent rook)))))))

(defmethod throw-bomb :around ((droid droid))
  (unless (clientp (current-buffer))
    (call-next-method)))

(defmethod update ((rook rook))
  (when (zerop (mod *updates* 30))
    (percent-of-time (+ 3 (by-level 4 5 5 5 5 5 5 6 6 6 6 6 7 7 8 8 8 9 9 9 9 9 9 10 10 10 10 10 10 10 10 11 11 12 12))
      (when (can-see-thing-p rook (nearest-agent rook))
	(walk-to-thing rook (nearest-agent rook))
	(funcall (random-choose (list #'attack #'attack #'throw-bomb)) rook))))
  (when (path-heading rook)
    (percent-of-time (by-level 1 1.1 1.1 1.1 1.2 1.2 1.3 1.3 1.3 1.4 1.4 1.5 1.5 1.6 1.7 1.7 1.7 1.8 1.9) (attack rook))
    (move rook (path-heading rook) (by-level 0.5 0.6 1 1.2 1.3 1.4 1.4 1.5 1.5 1.6 1.6 1.7 1.8)))
  (when (null (goal-x rook))
    (move-toward rook (slot-value rook 'direction)
		 (by-level 0.4 0.4 0.4 0.5 0.5 0.5 0.6 0.6 0.7 0.7 0.8 0.9 1 1 1 1.2 1.2 1.3 1.3 1.3 1.4 1.4 1.5 1.6 1.6 1.6 1.7))))

(defmethod collide :around ((rook rook) (wall wall))
  (if *sweep*
      (destroy rook)
      (call-next-method)))

;;; Security scan barrier

(defclass barrier (thing)
  ((image :initform "security.png")
   (direction :initform (random-choose '(:up :down)))))

(defmethod initialize-instance :after ((barrier barrier) &key)
  (resize barrier (units 16) (units 2)))

(defmethod update :before ((barrier barrier))
  (with-slots (x y width height color direction) barrier
    (setf color (random-choose '("cyan" "white" "yellow")))
    (when (minusp y)
      (mark-volatile barrier)
      (move-to barrier x 1)
      (setf direction :down))
    (when (> y (- *height* height))
      (mark-volatile barrier)
      (move-to barrier x (- *height* height 1))
      (setf direction :up))))

(defmethod draw ((barrier barrier))
  (with-slots (color image heading) barrier
    (multiple-value-bind (top left right bottom)
	(bounding-box barrier)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 :window-x (+ (shake) (units -1))
				 :window-y (+ (shake) (units -1))
				 ;; apply shading
				 :vertex-color color
				 :blend :additive)
      (set-blending-mode :alpha))))

(defmethod update ((barrier barrier))
  (with-slots (direction) barrier
    (move barrier (direction-heading direction)
	  (by-level 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2))))

(defmethod collide ((barrier barrier) (agent agent))
  (when (or (movement-direction agent)
	    (kicking-p agent))
    (die agent)))

(defmethod collide ((barrier barrier) (bullet bullet))
  (destroy bullet))

(defclass destructor (barrier)
  ((image :initform "security2.png")))

(defmethod collide ((destructor destructor) (agent agent))
  (die agent))

(defclass station (wall)
  ((image :initform "station.png")
   (hit-points :initform 10 :accessor hit-points)))

(defmethod update ((station station))
  (percent-of-time (by-level 2 2 3 3 4 4 4 4 4 4 5 5 5 5 6 6 6 6 7 7 7 7 8 8 8 8 9 9 9 10 10 10 11 11 11 12 12 12)
    (when (< (distance-between station (agent)) 500)
      (attack station))))

(defmethod collide ((enemy-bullet enemy-bullet) (station station)) nil)
(defmethod collide ((station station) (enemy-bullet enemy-bullet)) nil)

(defmethod initialize-instance :after ((station station) &key)
  (mark-volatile station)
  (resize station (units 5) (units 5)))

(defmethod draw ((station station))
  (with-slots (color image heading) station
    (multiple-value-bind (top left right bottom)
	(bounding-box station)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 :window-x (+ (shake) (units -1))
				 :window-y (+ (shake) (units -1))
				 ;; apply shading
				 :blend :alpha))))

(defmethod collide ((station station) (bullet bullet))
  (play-icon station (random-choose *blaagh-sounds*))
  (decf (hit-points station))
  (destroy bullet)
  (unless (plusp (hit-points station))
    (play-icon station "blurp.wav")
    (destroy station)))

(defmethod destroy :after ((station station))
  (mapc #'destroy (find-instances (arena) 'barrier)))

(defmethod attack ((station station))
  (multiple-value-bind (x y) (center-point station)
    (let* ((bullet (make-instance 'giga-bullet))
	   (x0 (- x (/ (slot-value bullet 'width) 2)))
	   (y0 (- y (/ (slot-value bullet 'height) 2))))
      (add-node (current-buffer) bullet x0 y0)
      (play-icon station (fire-sound bullet))
      (setf (slot-value bullet 'heading)
	    (or (percent-of-time 30 (heading-between station (agent)))
		(random (* 2 pi)))))))

;;; The arena

(defun level-enemy-classes* (level)
  (case (mod level 17)
    (0 '(spore))
    (1 '(droid biclops))
    (2 '(biclops))
    (3 '(droid biclops biclops))
    (4 '(droid))
    (5 '(biclops biclops monitor))
    (6 '(droid droid biclops))
    (7 '(droid biclops monitor))
    (8 '(biclops monitor biclops))
    (9 '(biclops monitor))
    (10 '(droid monitor))
    (11 '(biclops biclops mismunch monitor))
    (12 '(monitor mismunch))
    (13 '(biclops droid))
    (14 '(monitor monitor droid))
    (15 '(biclops monitor))
    (16 '(mismunch biclops))))

(defun level-enemy-classes (level)
  (let ((classes (level-enemy-classes* level)))
    (if (< level 20)
	classes
	(progn
	  (setf classes (delete (random-choose '(monitor mismunch biclops droid)) classes))
	  (setf classes (append classes classes classes classes))
	  (push
	   (if (< level 30)
	       (random-choose '(rook biclops))
	       (if (< level 50)
		   (if (evenp level) 'rook 'tracer)
		   (random-choose '(rook tracer))))
	   classes)
	  classes))))

;; (level-enemy-classes 25)
(defparameter *gradient-images* (image-set "gradient" 3))

(defclass arena (xelf:buffer)
  ((quadtree-depth :initform 8)
   (gradient :initform (random-choose *gradient-images*))
   (darkness-p :initform (dark-level-p *level*) :accessor darkness-p)))

;; (defmethod save-quadtree ((arena arena))
;;   (setf *cached-quadtree* (slot-value arena 'quadtree)))

(defmethod local-agent ((arena arena))
  (or (when (clientp arena)
	(sidekick))
      (agent)
      (first (find-instances arena 'agent))))

(defmethod quit-game ((arena arena)) (quit))

(defmethod handle-point-motion ((self arena) x y))
(defmethod press ((self arena) x y &optional button))
(defmethod release ((self arena) x y &optional button))
(defmethod tap ((self arena) x y))
(defmethod alternate-tap ((self arena) x y))

(defmethod draw :before ((arena arena))
  (draw-box (units -5) (units -5) (units (+ 10 (width-in-units))) (units (+ 10 (height-in-units))) :color *wall-color*)
  (draw-textured-rectangle 0 0 0 *width* *height* (find-texture (slot-value arena 'gradient))))
  ;; draw court lines
  ;; (draw-image "hoop.png" (- (units 3) 110) (- (units (/ (height-in-units) 2)) 110) :opacity 0.5 :height 200 :width 200))

(defresource "phosphor.png" :wrap-r :repeat :wrap-s :repeat)
(defresource "phosphor2.png" :wrap-r :repeat :wrap-s :repeat)
(defresource "phosphor3.png" :wrap-r :repeat :wrap-s :repeat)
(defresource "phosphor-tile-1.png" :wrap-r :repeat :wrap-s :repeat)
(defresource "phosphor-tile-2.png" :wrap-r :repeat :wrap-s :repeat)
(defresource "phosphor-tile-3.png" :wrap-r :repeat :wrap-s :repeat)

(defun draw-phosphor-effect (x y width height)
  (let* ((left x)
	 (right (+ x width))
	 (top y)
	 (bottom (+ y height))
	 (u1 left)
	 (v1 top)
	 (u2 (/ right 4))
	 (v2 (/ bottom 4)))
    (xelf::enable-texture-blending)	
    (set-blending-mode :multiply)
    (gl:bind-texture :texture-2d (find-texture "phosphor-tile-1.png"))
    (gl:matrix-mode :modelview)
    (xelf::set-vertex-color "white")
    (gl:with-pushed-matrix 
      (gl:load-identity)
      (gl:with-primitive :quads
	(gl:tex-coord u1 v2)
	(gl:vertex x bottom 0)
	(gl:tex-coord u2 v2)
	(gl:vertex right bottom 0)
	(gl:tex-coord u2 v1)
	(gl:vertex right y 0)
	(gl:tex-coord u1 v1)
	(gl:vertex x y 0)))))

(defmethod draw ((arena arena))
  (multiple-value-bind (top left right bottom) (window-bounding-box arena)
    (loop for object being the hash-keys of (slot-value arena 'objects) do
      ;; only draw onscreen objects
      (when (colliding-with-bounding-box-p (find-object object) top left right bottom)
	(draw (find-object object))))))

(defparameter *shake* 0)

(defparameter *shake-time* 8)

(defun shake-screen (&optional (time *shake-time*))
  (setf *shake* time))

(defun update-shake ()
  (when (plusp *shake*)
    (decf *shake*)))

(defun shake ()
  (if (or (zerop *shake*)
	  ;; disable shake on client for now
	  (clientp (current-buffer)))
      0
      (+ -2 (random 4))))

(defmethod draw :around ((arena arena))
  (transform-window :x (+ (shake) (units -1)) :y (+ (shake) (units -1.1)))
  (call-next-method)
  (transform-window :x 0 :y 0))

(defmethod draw :after ((arena arena))
  (mapc #'draw (find-instances arena 'barrier))
  (mapc #'draw (find-instances arena 'station))
  (when *darkness* 
    (multiple-value-bind (x y) (center-point (agent))
      (let ((q 1400))
	(draw-textured-rectangle (- x q) (- y q) 0 (* 2 q) (* 2 q)
				 (find-texture "darkness.png")))))
  (when (stringp *label*)
    (if (< *updates* 300)
	(draw-string *label* (units 32.5) (units 5) :color (random-choose '("white" "cyan")) :font *big-font*)
	(setf *label* nil)))
  (when (game-over-p)
    (draw-string "GAME OVER" (units 34.5) (units 5) :color (random-choose '("white" "cyan")) :font *big-font*))
  (when (and (not (game-over-p))
	     *paused*)
    (draw-string "PAUSED" (units 35.5) (units 5) :color (random-choose '("white" "cyan")) :font *big-font*))
  (set-blending-mode :multiply)
  (when *phosphor*
    (draw-textured-rectangle (units -40) (units -40) 0
			     (+ (* 1920 (/  *width*  *screen-width*)) (units 40))
			     (+ (* 1080 (/  *height* *screen-height*)) (units 40))
			     ;; *width* *height*
			     (find-texture "phosphor3.png") :blend :multiply))
    ;; (draw-phosphor-effect 0 0
			
  (set-blending-mode :alpha)
  (when (local-agent (arena))
    (when (powerup (local-agent (arena)))
      (draw-string (xelf::pretty-string (powerup (local-agent (arena))))
		   (units 42.5) (units -0.7)
		   :color (random-choose '("yellow" "magenta" "white" "cyan"))
		   :font *big-font*)))
  (draw-string (format nil " LEVEL: ~A" *level*)
	       (units 70.6) (units -0.7)
	       :color "white"
	       :font *big-font*)
  (draw-string (format nil " LIVES: ~A" (lives-remaining))
	       (units 61.6) (units -0.7)
	       :color "white"
	       :font *big-font*)
  (draw-string (format nil " OXYGEN: ")
	       (units 2.6) (units -0.7)
	       :color (if (low-oxygen-p (agent)) (random-choose '("red" "yellow")) "yellow")
	       :font *big-font*)
  (draw-box (units 9) (units -0.5) 400 (units 1) :color "gray20")
  (draw-box (units 9) (units -0.5) (* 4 (oxygen (agent))) (units 1)
	    :color (if (low-oxygen-p (agent))
		       (random-choose '("red" "yellow"))
		       (if (< (oxygen (agent)) 60) "yellow" "green")))
  (draw-string "[Arrows/NumPad/Joy] move   [Shift/JoyButton] fire   [Ctrl-R] reset game   [Ctrl-Q] quit game   [Ctrl-P] pause    [Ctrl-X] toggle shader   [Ctrl-M] music on/off   [Esc] setup"
	       (units 1.6) (- *height* 15)
	       :color "white"
	       :font *default-font*)
  ;; (when *netplay*
  ;;   (draw-string (ecase *netplay*
  ;; 		   (:client "CLIENT: PLAYER 2")
  ;; 		   (:server "SERVER: PLAYER 1"))
  ;; 		 (units 70) 3
  ;; 		 :color "cyan"
  ;; 		 :font *default-font*))
  ;; draw player overlay
  (dolist (agent (find-instances arena 'agent))
    (with-slots (shield-clock x y width height) agent
      (when (plusp shield-clock)
	(draw-string "invincible" (+ x 30) (+ y 2) :color (random-choose '("yellow" "magenta"))
						   :font *small-font*))
      (when (or (plusp shield-clock)
		(eq :invincible (powerup agent)))
	(draw-textured-rectangle x y 0 width height
				 (find-texture "shield.png")
				 :opacity 0.8
				 :vertex-color (random-choose '("yellow" "magenta")))))))


(defun make-wall (x y width height &optional (wall-class 'wall))
  (let ((wall (make-instance wall-class)))
    (resize wall width height)
    (move-to wall x y)
    wall))

(defun make-gate (x y width height)
  (make-wall x y width height 'gate))

(defun make-border (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width (units -1)))
	(bottom (+ y height (units -1))))
    (with-new-buffer
      ;; top wall
      (insert (make-wall left top (- right left) (units 1)))
      ;; bottom wall
      (insert (make-wall left bottom (- right left (units -1)) (units 1)))
      ;; left wall
      (insert (make-wall left top (units 1) (- bottom top)))
      ;; right wall
      (insert (make-wall right top (units 1) (- bottom top (units -1))))
      ;; send it back
      (current-buffer))))

(defun make-corridor (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width (units -1)))
	(bottom (+ y height (units -1))))
    (with-new-buffer
      ;; top wall
      (insert (make-wall left top (- right left) (units 1)))
      ;; (optional) gate
      (when *gates*
	(percent-of-time 80 (insert (make-gate (+ x (random (- width (units 2)))) (+ y (units 1)) (units 1) (- height (units 1))))))
      ;; bottom wall
      (insert (make-wall left bottom (- right left) (units 1)))
      (current-buffer))))

(defun make-corridor2 (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width (units -1)))
	(bottom (+ y height (units -1))))
    (with-new-buffer
      ;; left wall
      (insert (make-wall left top (units 1) (- bottom top)))
      ;; (optional) gate
      (when *gates*
	(percent-of-time 80 (insert (make-gate (+ left (units 1)) (+ y (random (- height (units 2)))) (- width (units 2)) (units 1)))))
      ;; right wall
      (insert (make-wall right top (units 1) (- bottom top)))
      (current-buffer))))

(defmethod initialize-instance :after ((arena arena) &key)
  (when (extra-life-level-p *level*)
    (extra-life))
  (bind-event arena '(:escape) 'setup)
  (bind-event arena '(:space) 'spacebar)
  (bind-event arena '(:return) 'spacebar)
  (bind-event arena '(:x :control) 'toggle-phosphor)
  (bind-event arena '(:m :control) 'toggle-music)
  (bind-event arena '(:p :control) 'pause-game)
  (bind-event arena '(:q :control) 'quit-game)
  (bind-event arena '(:r :control) 'reset-game)
  (bind-event arena '(:n :control) 'next-level)
  (setf *arena* arena)
  (resize arena *width* *height*)
  (setf *spore-instances* 0))

(defmethod toggle-phosphor ((arena arena))
  (setf *phosphor* (if *phosphor* nil t)))

(defmethod toggle-music ((arena arena))
  (setf *music* (if *music* nil t))
  (when (not *music*)
    (halt-music)))

(defun find-arena-class (&optional netplay)
  (case netplay
    (:client 'client-arena)
    (:server 'server-arena)
    (otherwise 'arena)))

(defmethod pause-game ((arena arena))
  (if (deadp (agent))
      (setf *paused* nil)
      (setf *paused* (if *paused* nil t))))

(defun make-corridor* (&rest args)
  (apply (random-choose (list #'make-corridor #'make-corridor2)) args))

(defun do-reset (&optional (level 0)) 
  (when *music*
    (play-music (nth (mod (truncate (/ level 2)) (length *tension-sounds*)) *tension-sounds*))
    (when (= *boss-level* level)
      (play-music "boss.ogg")))
  #+sbcl (sb-ext:gc :full t)
  (setf *wall-color* (random-wall-color))
  (let ((x (current-buffer)))
    (when x
      ;; (save-quadtree x)
      (at-next-update (destroy x))))
  (setf *level* level)
  (setf *gates* (gate-level-p level))
  (if (victory-level-p)
      (progn
	(stop (current-buffer))
	(switch-to-buffer (make-instance 'victory-screen)))
      (let ((arena (make-instance (find-arena-class *netplay*))))
	(setf xelf::*updates* 0)
	(setf *sweep* t)
	(switch-to-buffer arena)
	(populate arena)
	(when (serverp arena)
	  (add-node arena (make-instance 'level-icon))))))

(defun find-barrier-class ()
  (if (> *level* 25)
      (random-choose '(barrier destructor))
      'barrier))

(defun find-barrier-count ()
  (if (> *level* 19)
      2
      1))

(defun find-station-location ()
  (values
   (units (- (/ (width-in-units) 2) 2.5))
   (units (- (/ (height-in-units) 2) 2.5))))
   ;; (random-choose (list (units 3) (units (- (width-in-units) 8))))
   ;; (random-choose (list (units 3) (units (- (height-in-units) 8))))))

(defmethod populate ((arena arena))
  ;; add outer wall 
  (paste-from arena (make-border 0 0 *width* *height*))
  (assert (= 4 (hash-table-count (objects arena))))
  ;; add player
  (let ((far (percent-of-time 50 t)))
  (add-node arena (make-agent)
	    (if (co-op-p)
		(units (if (not far) 4
			   (- (width-in-units) 6)))
		(units (if (not far) 3
			   (- (width-in-units) 6))))
	    (units (/ (height-in-units) 2)))
  ;; possibly add sidekick
  (when (co-op-p)
    (add-node arena (make-sidekick)
	      (if (not far) (units 3) (units (- (width-in-units) 5)))
	      (units (+ 2 (/ (height-in-units) 2))))))
  (if (= *boss-level* *level*)
      ;; final boss
      (progn (paste-from arena (make-corridor* (units 23) (units 15) (units 32) (units 15)))
	     (paste-from arena (make-corridor* (units 13) (units 10) (units 50) (units 27)))
	     (add-node (current-buffer) (make-instance 'boss) 600 320))
      ;; maybe add hunter drone
      (progn 
	;; possibly add security system
	(when (security-level-p *level*)
	  (when (> *level* 19)
	    (multiple-value-bind (u v) (find-station-location)
	      (add-node arena (make-instance 'station) u v)))
	  (dotimes (n (find-barrier-count))
	    (add-node arena (make-instance (find-barrier-class)) (units (+ 14 (random 40))) (units (+ 10 (random 30))))))

	(when (> *level* 16)
	  (when (zerop (mod *level* 6))
	    (multiple-value-bind (x y)
		(values (units 45) (units 40))
	      (let ((drone (make-instance 'drone)))
		(add-node arena drone)
		(move-to drone x y)))))
	;; add default enemies
	(dotimes (n (+ 6 (by-level 2 2 3 4 5 6 6 7 7 7 8 8 8 9 9 10 10 10 11 11 11 12 12 12 13 13 13 14)))
	  (add-node arena (make-instance (random-choose (level-enemy-classes *level*)))
		    (units (+ 22 (random 35)))
		    (units (+ 4 (random 32))))))))
	;; ;; add walls and nubs
	;; (percent-of-time 80 (insert (make-wall (units 1) (units (+ 10 (random 10))) (units 1) (units 1))))
	;; (percent-of-time 80 (insert (make-wall (units 78) (units (+ 10 (random 10))) (units 1) (units 1))))
	;; ;; add side gates
	;; (when *gates*
	;;  (percent-of-time 80 (insert (make-gate (units 1) (units (+ 10 (random 30))) (units 14) (units 1))))
	;;  (percent-of-time 80 (insert (make-gate (units 66) (units (+ 10 (random 30))) (units 12) (units 1)))))
	;; ;; othernubs 
	;; (insert (make-wall (units (+ 10 (random 40))) (units 1) (units 1) (units 1))) 
	;; (insert (make-wall (units (+ 10 (random 40))) (units 43) (units 1) (units 1))) 
	;; ;;dividers
	;; (percent-of-time 75
	;; 		 (insert (make-wall (units (+ 6 (random 5))) (units (+ 15 (random 6))) (units 1) (units (+ 5 (random 13))))))
	;; (percent-of-time 75
	;; 		 (insert (make-wall (units (+ 68 (random 5))) (units (+ 15 (random 6))) (units 1) (units (+ 5 (random 13))))))
	;; ;; main corridors
	;; (percent-of-time 75
	;;   (paste-from arena (make-corridor* (units 15) (units 10) (units 50) (units 25))))
	;; ;;
	;; (percent-of-time 75
	;;   (paste-from arena (make-corridor* (units 23) (units 15) (units 32) (units 16))))
	;; ;;
	;; ;;
	;; (percent-of-time 75
	;;   (paste-from arena (make-corridor* (units 30) (units 19) (units 18) (units 8)))))))

(defmethod reset-game ((arena arena))
  (reset-lives)
  (dotimes (n 100)
    (halt-sample n))
  (at-next-update (do-reset 0)))

(defmethod next-level ((arena arena))
  (when (and (not (game-over-p))
	     (not (clientp arena)))
    (dotimes (n 100)
      (halt-sample n))
    (at-next-update (do-reset (1+ *level*)))))

(defmethod update :after ((arena arena))
  (update-shake)
  (when (deadp (agent))
    (update-retry-clock)
    (when (and (retry-ready-p)
	       (not (clientp arena))
	       (not (game-over-p)))
      (dotimes (n 100)
	(halt-sample n))
      (at-next-update (do-reset *level*))))
  (when (= xelf::*updates* 2)
    (play-sample "newball.wav"))
  (when (> xelf::*updates* 3)
    (setf *sweep* nil)
    (when
	(and (not (clientp arena))
	     (null (find-instances arena 'droid)))
      (at-next-update (do-reset (if (not *splash*)
				    (1+ *level*)
				    (progn
				      (setf *splash* nil)
				      (setf *level* (1+ *level*)))))))))

;;; Game victory

(defclass victory-screen (xelf:buffer)
  ((quadtree-depth :initform 4)
   (background-color :initform "CornflowerBlue")))

(defmethod initialize-instance :after ((victory-screen victory-screen) &key)
  (play-music "victory.ogg")
  (resize victory-screen *width* *height*))

(defun flicker () (random-choose '("white" "cyan")))

(defmethod draw :after ((victory-screen victory-screen))
  (draw-string "YOU BEAT THE FINAL BOSS!" (units 25.3) (units 2) :color (flicker) :font "sans-mono-bold-22"))

;;; Object insertion/removals

(defmethod remove-node :after ((arena arena) (droid droid))
  (when (and (not (clientp arena))
	     (not *sweep*))
    (play-icon droid "xplod.wav")))

(defmethod add-node :after ((buffer buffer) (bomb bomb) &optional x y z)
  (when (or (null *netplay*)
	    (not revive-p))
    (play-sample "bombs-away.wav")))

;;; Game setup

(defparameter *button-time* 30)
(defparameter *ready-time* 120)

(defclass setup (buffer)
  ((timer :initform 0)
   (player :initform 1)
   (quadtree-depth :initform 4)
   (background-color :initform "CornflowerBlue")))

(defmethod toggle-upnp ((setup setup))
  (setf *use-upnp* (if *use-upnp* nil t)))

(defmethod start-server ((setup setup))
  (play-inv8r :netplay :server 
	      :use-upnp *use-upnp*))

(defmethod start-client-prompt ((setup setup))
  (show-prompt))

(defmethod start-client ((setup setup))
  (play-inv8r :netplay :client 
	      :use-upnp *use-upnp* 
	      :server-host *server-host*))

(defmethod initialize-instance :after ((setup setup) &key)
  (setf *player-1-joystick* nil)
  (setf *player-2-joystick* nil)
  (setf *any-joystick* nil)
  (bind-event setup '(:s :control) 'start-server)
  (bind-event setup '(:c :control) 'start-client-prompt)
  (bind-event setup '(:u :control) 'toggle-upnp)
  (hide-terminal)
  (setf *inhibit-splash-screen* t)
  (resize setup *width* *height*))

(defmethod update :after ((setup setup))
  (with-fields (timer player) setup
    (when (plusp timer) 
      (decf timer))
    (when (and (zerop timer)
	       (null player))
      (stop setup)
      (if *player-2-joystick*
	  (setf *co-op-p* t)
	  (setf *co-op-p* nil))
      (do-reset))))

(defparameter *p1-prompt* "Press any button on Gamepad 1, or Spacebar to use the keyboard.")
(defparameter *p2-prompt-1* "To play co-op with a friend, press any button on Gamepad 2.")
(defparameter *p2-prompt-2* "Or, press any button on Gamepad 1 (or Spacebar) to play solo.")
(defparameter *press* "Press SPACEBAR or joystick button to begin!")

(defun ready-prompt ()
  (if *netplay*
      "Co-op netplay. Get ready!"
      (if (null *player-2-joystick*)
	  "Player 1. Get ready!"
	  "Player 1 and Player 2. Get Ready!")))
  
(defparameter *must* "(Gamepads must be plugged in before the application is started.)")
(defparameter *net* "Online Play: Press Control-S to start server,  Control-C for client. Press Control-U to toggle UPnP before starting.")

(defmethod draw :after ((setup setup))
  (draw-box (units -10) (units -10) 2300 2300 :color "CornflowerBlue")
  (when (null *prompt*)
    (with-fields (timer player) setup
      (draw-string "Game setup" (units 38.3) (units 2) :color "white" :font "sans-mono-bold-22")
      (draw-string *must* (units 28) (units 5) :color "white" :font "sans-mono-bold-12")
      (draw-string (if *use-upnp* "UPnP Enabled" "UPnP Disabled") (units 39) (units 9) :color "white" :font "sans-mono-bold-12")
      (draw-string 
       (if *netplay* 
	   (format nil "Online play enabled in ~S mode." *netplay*)
	   *net*)
       (units 18) (units 7) :color "white" :font "sans-mono-bold-12")
      (case player 
	(1 (draw-string *p1-prompt* (units 20) (units 12) :color (flicker) :font *big-font*))
	(2 (draw-string *p2-prompt-1* (units 20) (units 14) :color (flicker) :font *big-font*)
	 (draw-string *p2-prompt-2* (units 20) (units 16) :color "white" :font *big-font*))
	(3 (draw-string *press* (units 20) (units 16) :color (flicker) :font *big-font*)))
      (when (null player)
	(draw-string (ready-prompt) (units 20) (units 18) :color "white" :font *big-font*))))
  (when *prompt*
    (draw *prompt*)))

(defmethod handle-event :after ((setup setup) event)
  (with-fields (timer player) setup
    (when (and (consp (first event))
	       (eq :space (first (first event))))
      (setf timer *ready-time*)
      (when (numberp player)
	(cond ((= player 2)
	       (setf *co-op-p* nil player 3))
	      ((= player 1)
	       (setf player 2))
	      ((= 3 player)
	       (setf player nil)))))
    (when (and (eq :joystick (first event))
	       (not (plusp timer)))
      (destructuring-bind (which button direction) (rest event)
	(case player
	  (1 
	   (if (not (clientp (arena)))
	       (progn (setf *player-1-joystick* which)
		      (setf *player-2-joystick* nil)
		      (setf timer *button-time*)
		      (setf player 2))
	       (progn (setf *player-1-joystick* nil)
		      (setf *player-2-joystick* which)
		      (setf timer *ready-time*)
		      (setf player nil))))
	  (2 
	   (if (and (numberp *player-1-joystick*)
		    (= which *player-1-joystick*))
	       (progn 
		 (setf *player-2-joystick* nil)
		 (setf timer *ready-time*)
		 (setf player 3))
	       (progn 
		 (setf *player-2-joystick* which)
		 (setf timer *ready-time*)
		 (setf player 3))))
	  (3
	   (setf timer *ready-time*)
	   (setf player nil)))))))

(defmethod handle-event :around ((setup setup) event)
  (if *prompt*
      (prog1 t (handle-event *prompt* event))
      (call-next-method)))

(defmethod setup ((arena arena))
  (stop arena)
  (at-next-update 
    (switch-to-buffer (make-instance 'setup))
    (destroy arena)))

;;; Preventing mousey movements

(defmethod handle-point-motion ((self arena) x y))
(defmethod press ((self arena) x y &optional button))
(defmethod release ((self arena) x y &optional button))
(defmethod tap ((self arena) x y))
(defmethod alternate-tap ((self arena) x y))

;;; Prompt widget

(defclass ip-prompt (prompt)
  ((prompt-string :initform "Type the server IP address and then press ENTER.")))

(defmethod read-expression ((prompt ip-prompt) input-string)
  input-string)

(defmethod enter :before ((prompt ip-prompt) &optional no-clear)
  (handler-case 
      (let ((*read-eval* nil))
	(let ((result (parse-ip (slot-value prompt 'line))))
	  (if (null result)
	      (logging "Error: not a valid IP address.")
	      (progn 
		(setf *server-host* (reformat-ip result))
		(start-client (current-buffer))))))
    (condition (c)
      (logging "~S" c))))

(defun show-prompt ()
  (show-terminal)
  (setf *prompt* (make-instance 'ip-prompt))
  (move-to *prompt* *terminal-left* *terminal-bottom*))

(defun hide-prompt ()
  (setf *prompt* nil))

(defmethod print-object ((path xelf::path) stream)
  (format stream "#<XELF:PATH>"))

;;; Netplay integration

(defmethod collide :around ((this bullet) (that droid))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod collide :around ((this droid) (that bullet))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod collide :around ((bomb bomb) (agent agent))
  (unless (clientp (current-buffer))
    (call-next-method)))

(defmethod collide :around ((agent agent) (explosion explosion))
  (unless (clientp (current-buffer))
    (call-next-method)))

(defmethod collide :around ((agent agent) (bomb bomb))
  (unless (clientp (current-buffer))
    (call-next-method)))

(defmethod collide :around ((explosion explosion) (agent agent))
  (unless (clientp (current-buffer))
    (call-next-method)))

;; (defmethod quadrille:find-identifier ((thing thing))
;;   (xelf:make-keyword (xelf:uuid thing)))

;; (defun find-thing-from-id (id)
;;   (xelf:find-object (symbol-name id) t))

;; (setf quadrille:*identifier-search-function* 
;;       #'find-thing-from-id)

(setf xelf:*game-variables* '(xelf:*updates* *co-op-p* *label*
*paused* *darkness* *retries* *shake-time* *wall-color* *level*
*gradient* *highest-level*))

(setf xelf:*object-variables* '(*agent* *sidekick*))

(setf xelf:*safe-variables* 
      (append *game-variables* 
	      *object-variables* 
	      xelf:*other-variables*))

(setf xelf:*terminal-bottom* (- *height* (units 1.5)))

(setf xelf:*prompt-font* xelf:*terminal-font*)

(defmethod find-player ((arena arena) n)
  (ecase n
    (1 (agent))
    (2 (sidekick))))

(defmethod spacebar ((arena arena)) 
  (hide-terminal))

(defclass client-arena (client-buffer arena) ())

(defmethod initialize-instance :after ((arena client-arena) &key)
  (setf (slot-value arena 'quadtree) nil)
  (install-quadtree arena)
  (show-prompt))

(defmethod proceed ((arena client-arena)) 
  (play-sample "go.wav"))

(defmethod populate ((arena client-arena))
  nil)

(defmethod find-input ((agent agent))
  (list 
   :time (current-time)
   :player-id (slot-value agent 'player-id)
   :direction (arrow-keys-direction)
   :kicking-p (kicking-p agent)))

(defmethod find-local-inputs ((arena client-arena))
  (when (find-instances arena 'sidekick)
    (list (find-input (first (find-instances arena 'sidekick))))))

(defmethod update-input-state :after ((agent agent) plist time)
  (destructuring-bind (&key direction kicking-p player-id time) plist
    (setf (input-direction agent) direction)
    (setf (input-kicking-p agent) kicking-p)))

(defmethod kicking-p ((hero hero))
  (when (not (clientp (current-buffer)))
    (holding-fire-p hero)))

(defmethod kicking-p ((sidekick sidekick))
  (if (not *netplay*)
      (when *player-2-joystick* 
	(holding-button-p *player-2-joystick*))
      (if (clientp (arena))
	  (or (holding-shift-p)
	      (when *player-2-joystick* 
		(holding-button-p *player-2-joystick*)))
	  (when (and (serverp (arena))
		     (input-p sidekick)
		     (input-update-p sidekick))
	    (input-kicking-p sidekick)))))

(defmethod stick-heading :around ((agent agent))
  (if (serverp (arena))
      (when (input-direction agent)
	(direction-heading (input-direction agent)))
      (call-next-method)))

(defmethod movement-direction ((hero hero))
  (when (not (deadp hero))
    (or (when (or (null *netplay*)
		  (serverp (current-buffer)))
	  (arrow-keys-direction))
	(when (stick-heading hero)
	  (stick-direction hero)))))

(defmethod stick-heading ((sidekick sidekick))
  (when (or (when (any-joystick) 0) *player-2-joystick*)
    (when (left-analog-stick-pressed-p (or (when (any-joystick) 0) *player-2-joystick*))
      (left-analog-stick-heading (or (when (any-joystick) 0) *player-2-joystick*)))))

(defmethod stick-heading :around ((sidekick sidekick))
  (if (serverp (arena))
      (when (input-direction sidekick)
	(direction-heading (input-direction sidekick)))
      (call-next-method)))

(defmethod movement-direction :around ((sidekick sidekick))
  (when (not (deadp sidekick))
    (or (when (clientp (arena))
	  (arrow-keys-direction))
	(call-next-method))))

(defmethod initialize-instance :after ((sidekick sidekick) &key)
  (setf *sidekick* sidekick))

(defmethod serve :around ((sidekick sidekick))
  (when (not (clientp (arena)))
    (call-next-method)))

(defmethod initialize-instance :after ((hero hero) &key)
  (setf *agent* hero))

(defmethod collide :around ((thing thing) (hero hero))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod collide :around ((hero hero) (thing thing))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod collide :around ((thing thing) (sidekick sidekick))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod collide :around ((sidekick sidekick) (thing thing))
  (unless (clientp (arena))
    (call-next-method)))

(defmethod find-netplay-joystick ((arena arena)) nil)
(defmethod find-netplay-id ((arena arena)) nil)

(defclass server-arena (server-buffer arena) ())

(defmethod find-netplay-joystick ((arena server-arena)) *player-1-joystick*)
(defmethod find-netplay-joystick ((arena client-arena)) *player-2-joystick*)

(defmethod find-netplay-id ((arena server-arena)) 1)
(defmethod find-netplay-id ((arena client-arena)) 2)

(defmethod make-census ((arena arena))
  (let ((uuids (make-hash-table :test 'equal :size 64)))
    (do-nodes (node arena)
      (unless (typep node (find-class 'spark))
	(setf (gethash (slot-value node 'xelf::uuid) uuids)
	      (slot-value node 'xelf::uuid))))
    (setf *census* uuids)
    ;; (verbosely "Created census with ~S/~S uuids." (hash-table-count uuids)
    ;; 	       (length (get-nodes arena)))
    uuids))

(defmethod clean-buffer ((arena arena))
  (let ((census *census*)
	(count 0))
    (when census
      (progn (do-nodes (node arena)
	       (when (not (gethash (slot-value node 'xelf::uuid) 
				   census))
		 (unless (or (typep node (find-class 'explosion))
			     (typep node (find-class 'glow))
			     (typep node (find-class 'spark)))
		   (remove-node arena node)
		 (incf count))))
	     (setf *census* nil)))))

(defmethod background-stream ((arena server-arena))
  (mapc #'(lambda (x) (slot-value x 'xelf::uuid))
  	(nconc (find-instances arena 'wall)
	       (find-instances arena 'station)
	       (find-instances arena 'barrier)
  	       (find-instances arena 'droid))))

(defmethod ambient-stream ((arena server-arena))
  (mapc #'(lambda (x) (slot-value x 'xelf::uuid))
	(nconc (find-instances arena 'wall)
	       (find-instances arena 'station)
	       (find-instances arena 'station)
	       (find-instances arena 'barrier)
	       (find-instances arena 'droid))))

(defmethod update :before ((arena arena))
  (setf *darkness* (darkness-p arena)))

(defmethod update :before ((arena client-arena))
  (setf (darkness-p arena) *darkness*))

(defun find-new-icons ()
  (let ((icons (find-instances (arena) 'icon)))
    (remove-if #'triggered-p icons)))

(defmethod update :after ((arena server-arena))
  (setf *gradient* (slot-value arena 'gradient))
  (when (connectedp arena)
    (when (evenp *updates*)
      ;; (dolist (n
      ;; 	       (nconc (find-instances arena 'droid)
      ;; 		      (find-instances arena 'powerup)
      ;; 		      (find-instances arena 'bomb)
      ;; 		      (find-instances arena 'trail)
      ;; 		      (find-instances arena 'explosion)
      ;; 		      (find-instances arena 'barrier)
      ;; 		      (find-instances arena 'icon)
      ;; 		      (find-instances arena 'oxygen-tank)
      ;; 		      (find-instances arena 'bullet)))
      (do-nodes (n arena)
	(when (volatile-p n)
	  (send-node n))))))

(defmethod update :around ((arena client-arena))
  (when *gradient* (setf (slot-value arena 'gradient) *gradient*))
  (with-buffer arena
    ;; (do-nodes (node arena)
    ;;   (when (or (typep node (find-class 'bullet))
    ;; 		(typep node (find-class 'bomb))
    ;; 		(typep node (find-class 'droid)))
    ;; 	(update node)))
    ;; (mapc #'update (find-instances arena 'explosion))
    ;; (mapc #'update (find-instances arena 'spark))
    ;; (mapc #'update (find-instances arena 'glow))
    ;; (mapc #'update (find-instances arena 'icon))
    (call-next-method)))

;;; Main program

(defun play-inv8r (&key (use-upnp nil) (netplay *netplay*) (server-host *server-host*) (client-host *client-host*) (base-port *base-port*) verbose-logging)
  (reset-lives)
  (setf *co-op-p* t)
  (when netplay 
    (close-netplay))
  (setf xelf::*use-upnp* use-upnp)
  (setf xelf::*burst-period* 2)
  (setf *degrade-stream-p* nil)
  (setf *server* nil)
  (setf *client* nil)
  (setf *verbose-p* verbose-logging)
  (setf *server-port* (or base-port *base-port*))
  (setf *netplay* netplay)
  (setf *server-host* server-host)
  (setf *client-host* client-host)
  (setf *sent-messages-count* 0)
  (setf *received-messages-count* 0)
  (setf *remote-host* nil)
  (setf *remote-port* nil)
  (setf *last-message-timestamp* 0)
  (setf *flag-received-p* nil)
  (setf *flag-sent-p* nil)
  ;;
  ;; (unless (or *netplay* *inhibit-splash-screen*)
  ;;   (logging "PRESS [ESCAPE] KEY TO CONFIGURE PLAYERS, GAMEPADS, AND NETWORKING."))
  (setf *player-1-joystick* nil)
  (setf *player-2-joystick* nil)
  (setf *any-joystick* t)
  (disable-key-repeat)
  (do-reset 0))

(defun inv8r (&optional (lev 0))
  (reset-lives)
  (when *netplay* (close-netplay))
  (setf *gradient* nil)
  (setf *triggers* (make-hash-table :test 'equal))
  (setf *netplay* nil)
  (setf *co-op-p* nil)
  (setf *gates* nil)
  (setf *player-2-joystick* nil)
  (setf *phosphor* t)
  (setf *music* t)
  (setf *highest-level* 0)
  (setf *label* nil)
  (setf *cached-quadtree* nil)
  (setf *paused* nil)
  (setf *darkness* nil)
  (setf *splash* t)
  (setf *level* 0)
  (setf xelf::*updates* 0)
  (setf *sweep* nil)
  (setf *joystick-enabled* nil)
  (setf *window-title* "inv8r v2.5")
  (setf *use-antialiased-text* nil)
  (setf *scale-output-to-window* t) 
  (setf *frame-rate* 60)
  (setf *shake* 0)
  (setf *screen-width* 1280)
  (setf *screen-height* 720)
  ;; (setf *nominal-screen-width* 1280)
  ;; (setf *nominal-screen-height* 720)
  (setf *nominal-screen-width* (truncate (* 1280 1.028)))
  (setf *nominal-screen-height* (truncate (* 720 1.05)))
  (setf *default-font* "sans-mono-bold-12")
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :mipmap)
  (setf *default-texture-filter* :mipmap)
  (setf *inhibit-splash-screen* nil)
  (with-session
      (open-project "inv8r")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (clear-terminal)
    (dolist (line (split-string-on-lines *inv8r-copyright-notice*))
      (logging line))
    (show-terminal)
    (preload-resources)
    (do-reset lev)))

  
;;; inv8r.lisp ends here
